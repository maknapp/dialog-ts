# Install
``
npm install git@gitlab.com:maknapp/dialog-ts.git
``

## Adapter
- [Golang](https://gitlab.com/maknapp/dialog-go)
- [PHP](https://gitlab.com/maknapp/dialog-php)