module Maknapp{
    export module Dialog {
        const ERROR_MSG_REQUIRED = 'This field is required';

        interface currency {
            Value: number,
            Currency: string,
        }

        interface location {
            Latitude: number,
            Longitude: number,
        }


        class Requirement {
            private dialog: Maknapp.Dialog.Dialog;

            public name: string;
            public pattern: string;
            public value: string;
            public greater: string;
            public less: string;

            constructor(dialog: Dialog) {
                this.dialog = dialog;
            }

            check(): boolean {
                let field = this.dialog.get(this.name);

                if(typeof field === "undefined"){
                    console.error('dialog check can not find field ' + this.name);
                    return true;
                }

                let value = field.value();
                if(field instanceof FieldText){
                    let check = true;

                    if(typeof this.value !== 'undefined') {
                        check = value === this.value;
                    }
                    if(check && typeof this.pattern !== 'undefined'){
                        let regex = new RegExp(this.pattern);
                        check = regex.test(value);
                    }

                    return check;
                } else if(field instanceof FieldSwitch){
                    return value.toString() === this.value;
                } else if(field instanceof FieldNumber){
                    let check = true;

                    if(typeof this.value !== 'undefined') {
                        check = value === this.value;
                    }
                    if(check && typeof this.pattern !== 'undefined'){
                        let regex = new RegExp(this.pattern);
                        check = regex.test(value);
                    }
                    if(check && typeof this.greater !== 'undefined'){
                        check = value > this.greater;
                    }
                    if(check && typeof this.less !== 'undefined'){
                        check = value < this.less;
                    }

                    return check;
                } else if(field instanceof FieldSelect){
                    return field.value() === this.value;
                }
                // todo: req check Date, Time, Datetime, Currency, Location, File

                return true;
            }
        }

        class RequirementAND {
            readonly requirements: Array<Requirement|RequirementOR>;

            constructor(items: Array<Requirement|RequirementOR>) {
                this.requirements = items;
            }

            check(): boolean{
                if(this.requirements.length === 0){
                    return true
                } else {
                    for(let req of this.requirements){
                        if(!req.check()) return false;
                    }
                    return true;
                }
            }

            appendRequirement(item: Requirement|RequirementOR){
                this.requirements.push(item);
            }
        }

        class RequirementOR {
            private readonly requirements: Array<Requirement|RequirementAND>;

            constructor(items: Array<Requirement|RequirementAND>) {
                this.requirements = items;
            }

            check(): boolean{
                if(this.requirements.length === 0){
                    return true
                } else {
                    for(let req of this.requirements){
                        if(req.check()) return true;
                    }
                    return false;
                }
            }

            appendRequirement(item: Requirement|RequirementAND){
                this.requirements.push(item);
            }
        }

        function getDomPath(el) {
            let stack = [el];
            while ( el.parentNode != null ) {
                let sibCount = 0;
                let sibIndex = 0;
                for ( let i = 0; i < el.parentNode.childNodes.length; i++ ) {
                    let sib = el.parentNode.childNodes[i];
                    if ( sib.nodeName == el.nodeName ) {
                        if ( sib === el ) {
                            sibIndex = sibCount;
                        }
                        sibCount++;
                    }
                }
                stack.push(el);
                el = el.parentNode;
            }
            return stack.slice(1); // removes the html element
        }

        export class Dialog {
            public Views: Array<View> = [];
            private Fields: Array<Field> = [];
            private element: HTMLDivElement;
            private viewContainer: HTMLDivElement;
            private headline: HTMLHeadingElement;
            private mobileClose: HTMLDivElement;

            public viewHistory: Array<string> = [];
            protected viewCurrent: string;
            private initValues;
            private initView: string;
            private errorMsg: HTMLParagraphElement;

            constructor() {
                this.init();

                // Listener
                window.addEventListener('keydown', function(event: KeyboardEvent){
                    if(event.key === "Escape" && document.body.contains(this.element)) {
                        this.close();
                    }
                }.bind(this), false);

                document.body.addEventListener('mousedown', function(event: PointerEvent){
                    if(document.body.contains(this.element) && getDomPath(event.target).indexOf(this.element) === -1 ) {
                        this.close();
                    }
                }.bind(this));
            }

            init(){
                this.element = document.createElement('div');
                this.element.className = "dialog";

                this.headline = document.createElement('h1');
                this.element.appendChild(this.headline);

                this.mobileClose = document.createElement('div');
                this.mobileClose.className = 'mobile-close';
                this.element.appendChild(this.mobileClose);

                this.errorMsg = document.createElement('p');
                this.errorMsg.className = 'errorMsg';
                this.element.appendChild(this.errorMsg);

                this.viewContainer = document.createElement('div');
                this.viewContainer.className = 'view-container';
                this.element.appendChild(this.viewContainer);

                // mobile close
                this.mobileClose.addEventListener('click', function(){this.close()}.bind(this));
            }

            set(name: string, obj: Field) {
                this.Fields[name] = obj;
            }

            setTitle(title: string) {
                this.headline.innerText = title;
            }

            setView(viewName: string, pushOnHistory: boolean = true){
                if(typeof this.Views[viewName] !== 'undefined') {
                    while(this.viewContainer.firstChild) this.viewContainer.removeChild(this.viewContainer.firstChild);
                    this.viewContainer.appendChild(this.Views[viewName].get());
                    if(pushOnHistory) this.viewHistory.push(this.viewCurrent);
                    this.viewCurrent = viewName;

                    this.Views[viewName].onShow();
                }
            }

            remove(name: string){
                delete this.Fields[name];
            }

            get(name: string): Field|undefined {
                return this.Fields[name];
            }

            showError(error: string){
                this.errorMsg.innerText = error;
            }

            value(): object {
                let values = {};
                for(let key in this.Fields){
                    if(this.Fields[key] instanceof Field){
                        let val = this.Fields[key].value();
                        if(typeof val !== "undefined"){
                            values[key] = this.Fields[key].value();
                        }
                    }
                }

                return values;
            }

            requirementsCheck() {
                // Fields
                for(let key in this.Fields){
                    if(this.Fields[key] instanceof Field){
                        this.Fields[key].checkRequirements();
                    }
                }

                // Buttons
                for(let viewKey in this.Views){
                    for(let key in this.Views[viewKey].buttons){
                        this.Views[viewKey].buttons[key].checkRequirements();
                    }
                }
            }

            load(path: string) {
                fetch(path).then((response) => response.text()).then((data) => {
                    let parser = new DOMParser();
                    let xmlDoc = parser.parseFromString (data, 'text/xml');

                    this.parseXML(xmlDoc);
                    this.initValues = this.value();
                    this.requirementsCheck();
                    this.show();
                })
            }

            parseXML(xmlDoc: XMLDocument){
                let xmlDialog = xmlDoc.getElementsByTagName('dialog')[0];

                // <dialog> attributes
                this.setTitle(xmlDialog.getAttribute('title'));
                if(xmlDialog.hasAttribute('view')){
                    this.initView = xmlDialog.getAttribute('view');
                }

                let xmlViews = xmlDialog.children;
                for(let xmlView of xmlViews){
                    let view = new View(this);

                    // <view> attributes
                    if(xmlView.hasAttribute('name')){
                        view.setName(xmlView.getAttribute('name'));
                    }

                    let xmlContents = xmlView.children;
                    for(let xmlContent of xmlContents) {
                        switch (xmlContent.tagName) {
                            case "html":
                                let contentHTML = new ContentHTML(xmlContent.innerHTML.trim());
                                view.appendContent(contentHTML);
                                break
                            case "form":
                                let contentForm = new ContentForm();

                                if(xmlContent.hasAttribute('orientation') && xmlContent.getAttribute('orientation') === 'horizontal'){
                                    contentForm.setOrientation('horizontal');
                                }

                                let fields = xmlContent.children
                                for(let fieldXML of fields){
                                    let field: Field;
                                    switch (fieldXML.tagName) {
                                        case "text":
                                            field = new FieldText(view);
                                            break
                                        case "number":
                                            field = new FieldNumber(view);
                                            break
                                        case "file":
                                            field = new FieldFile(view);
                                            break
                                        case "date":
                                            field = new FieldDate(view);
                                            break
                                        case "time":
                                            field = new FieldTime(view);
                                            break
                                        case "datetime":
                                            field = new FieldDatetime(view);
                                            break
                                        case "switch":
                                            field = new FieldSwitch(view);
                                            break
                                        case "select":
                                            field = new FieldSelect(view);
                                            break
                                        case "currency":
                                            field = new FieldCurrency(view);
                                            break
                                        case "location":
                                            field = new FieldLocation(view);
                                            break
                                        case "address":
                                            field = new FieldAddress(view);
                                            break
                                        case "fileSelect":
                                            field = new FieldFileSelector(view);
                                            break
                                        default:
                                            console.error('field type `'+fieldXML.tagName+'` unknown')
                                            continue
                                    }

                                    // Name & Label
                                    if(!fieldXML.hasAttribute('name') && !fieldXML.hasAttribute('label')){
                                        console.error('dialog fields must have a name and a label')
                                        continue
                                    }
                                    field.setName(fieldXML.getAttribute('name'));
                                    field.setLabel(fieldXML.getAttribute('label'));

                                    // Required
                                    if(fieldXML.hasAttribute('required')){
                                        field.setRequired(fieldXML.getAttribute('required') === 'true');
                                    }

                                    // Hint
                                    if(fieldXML.hasAttribute('hint')){
                                        field.setHint(fieldXML.getAttribute('hint'));
                                    }

                                    // Visibility
                                    if(fieldXML.hasAttribute('visibility')){
                                        field.setVisibility(fieldXML.getAttribute('visibility') === 'true');
                                    }

                                    // Requirements
                                    let reqStack = [new RequirementAND([])];
                                    this.parseXMLRequirements(fieldXML, reqStack);
                                    if(reqStack[0].requirements.length > 0) {
                                        field.setRequirements(reqStack[0])
                                    }

                                    // Prefix & Suffix
                                    if(field instanceof FieldText
                                        || field instanceof FieldNumber
                                        || field instanceof FieldFile
                                        || field instanceof FieldSwitch
                                        || field instanceof FieldCurrency
                                    ) {
                                        if(fieldXML.hasAttribute('prefix')){
                                            field.setPrefix(fieldXML.getAttribute('prefix'));
                                        }
                                        if(fieldXML.hasAttribute('suffix')){
                                            field.setSuffix(fieldXML.getAttribute('suffix'));
                                        }
                                    }

                                    // Placeholder
                                    if(field instanceof FieldText
                                        || field instanceof FieldNumber
                                        || field instanceof FieldFile
                                        || field instanceof FieldCurrency
                                    ){
                                        if(fieldXML.hasAttribute('placeholder')){
                                            field.setPlaceholder(fieldXML.getAttribute('placeholder'));
                                        }
                                    }

                                    // Text Length
                                    if(field instanceof FieldText){
                                        if(fieldXML.hasAttribute('maxLength')){
                                            field.setMaxLength(Number(fieldXML.getAttribute('maxLength')));
                                        }

                                        if(fieldXML.hasAttribute('check')){
                                            field.setCheck(fieldXML.getAttribute('check'), fieldXML.getAttribute('checkHint'));
                                        }
                                    }

                                    // Number min, max, step
                                    if(field instanceof FieldNumber
                                        || field instanceof FieldCurrency
                                    ){
                                        if(fieldXML.hasAttribute('min')){
                                            field.setMin(Number(fieldXML.getAttribute('min')));
                                        }
                                        if(fieldXML.hasAttribute('max')){
                                            field.setMax(Number(fieldXML.getAttribute('max')));
                                        }
                                        if(fieldXML.hasAttribute('step')){
                                            field.setStep(Number(fieldXML.getAttribute('step')));
                                        }
                                    }

                                    // Location
                                    if(field instanceof FieldLocation){
                                        if(fieldXML.hasAttribute('map')){
                                            field.setMap(fieldXML.getAttribute('map') === 'true');
                                        }
                                    }

                                    // currency: <item>
                                    if(field instanceof FieldCurrency){
                                        let currencies = [];
                                        for(let itemXML of fieldXML.children){
                                            if(itemXML.tagName === 'item'){
                                                currencies.push(itemXML.textContent);
                                            }
                                        }
                                        field.setCurrencies(currencies);
                                    }

                                    // select: <option>
                                    if(field instanceof FieldSelect){
                                        if(fieldXML.hasAttribute('type') && fieldXML.getAttribute('type') === 'search'){
                                            field.setType('search');
                                        }

                                        if(fieldXML.hasAttribute('source')){
                                            field.setSource(fieldXML.getAttribute('source'));
                                        }

                                        for(let optionXML of fieldXML.children){
                                            if(optionXML.tagName === 'option'){
                                                let item: listItem = {
                                                    headline: '',
                                                    value: ''
                                                }
                                                if(!optionXML.hasAttribute('value')){
                                                    console.error('select option must have a value')
                                                    continue
                                                }
                                                item.value = optionXML.getAttribute('value');
                                                item.headline = optionXML.textContent;

                                                field.appendItem(item);
                                            }
                                        }
                                    }

                                    // Address
                                    if(field instanceof FieldAddress){
                                        if(fieldXML.hasAttribute('streetNumber')){
                                            field.setStreetNumber(Number(fieldXML.getAttribute('streetNumber')));
                                        }

                                        if(fieldXML.hasAttribute('houseName')){
                                            field.setHouseName(fieldXML.getAttribute('houseName'));
                                        }

                                        if(fieldXML.hasAttribute('streetNumberSuffix')){
                                            field.setStreetNumberSuffix(fieldXML.getAttribute('streetNumberSuffix'));
                                        }

                                        if(fieldXML.hasAttribute('streetName')){
                                            field.setStreetName(fieldXML.getAttribute('streetName'));
                                        }

                                        if(fieldXML.hasAttribute('streetType')){
                                            field.setStreetType(fieldXML.getAttribute('streetType'));
                                        }

                                        if(fieldXML.hasAttribute('streetDirection')){
                                            field.setStreetDirection(fieldXML.getAttribute('streetDirection'));
                                        }

                                        if(fieldXML.hasAttribute('addressType')){
                                            field.setAddressType(fieldXML.getAttribute('addressType'));
                                        }

                                        if(fieldXML.hasAttribute('addressTypeIdentifier')){
                                            field.setAddressTypeIdentifier(fieldXML.getAttribute('addressTypeIdentifier'));
                                        }

                                        if(fieldXML.hasAttribute('localMuniciplity')){
                                            field.setLocalMuniciplity(fieldXML.getAttribute('localMuniciplity'));
                                        }

                                        if(fieldXML.hasAttribute('city')){
                                            field.setCity(fieldXML.getAttribute('city'));
                                        }

                                        if(fieldXML.hasAttribute('governingDistrict')){
                                            field.setGoverningDistrict(fieldXML.getAttribute('governingDistrict'));
                                        }

                                        if(fieldXML.hasAttribute('postalArea')){
                                            field.setPostalArea(fieldXML.getAttribute('postalArea'));
                                        }

                                        if(fieldXML.hasAttribute('country')){
                                            field.setCountry(fieldXML.getAttribute('country'));
                                        }
                                    }



                                    // file selector
                                    if(field instanceof FieldFileSelector) {
                                        if (fieldXML.hasAttribute('source')) {
                                            field.setSource(fieldXML.getAttribute('source'));
                                        }
                                    }

                                    // set Value
                                    if(field instanceof FieldText
                                        || field instanceof FieldDate
                                        || field instanceof FieldTime
                                        || field instanceof FieldDatetime
                                        || field instanceof FieldFileSelector
                                    ){
                                        if(fieldXML.hasAttribute('value')){
                                            field.setValue(fieldXML.getAttribute('value'));
                                        }
                                    } else if (field instanceof FieldNumber) {
                                        if(fieldXML.hasAttribute('value')){
                                            field.setValue(Number(fieldXML.getAttribute('value')));
                                        }
                                    } else if (field instanceof FieldSwitch) {
                                        if(fieldXML.hasAttribute('value')){
                                            field.setValue(fieldXML.getAttribute('value') === 'true');
                                        }
                                    } else if (field instanceof FieldSelect) {
                                        if(fieldXML.hasAttribute('value')){
                                            field.setValue(fieldXML.getAttribute('value'));
                                        }
                                    } else if (field instanceof FieldCurrency) {
                                        if(fieldXML.hasAttribute('value') && fieldXML.hasAttribute('currency')){
                                            field.setValue({
                                                Value: Number(fieldXML.getAttribute('value')),
                                                Currency: fieldXML.getAttribute('currency'),
                                            });
                                        }
                                    } else if (field instanceof FieldLocation) {
                                        if(fieldXML.hasAttribute('latitude') && fieldXML.hasAttribute('longitude')){
                                            field.setValue({
                                                Latitude: Number(fieldXML.getAttribute('latitude')),
                                                Longitude: Number(fieldXML.getAttribute('longitude')),
                                            });
                                        }
                                    }

                                    contentForm.appendField(field);
                                }
                                view.appendContent(contentForm);
                                break
                            case 'button':
                                let button = new Button(this);
                                if(xmlContent.hasAttribute('label')){
                                    button.setLabel(xmlContent.getAttribute('label'));
                                }
                                if(xmlContent.hasAttribute('action')){
                                    button.setAction(xmlContent.getAttribute('action'));
                                }
                                if(xmlContent.hasAttribute('visibility')){
                                    button.setVisibility(xmlContent.getAttribute('visibility') === 'true');
                                }
                                // Requirements
                                let reqStack = [new RequirementAND([])];
                                this.parseXMLRequirements(xmlContent, reqStack);
                                if(reqStack[0].requirements.length > 0) {
                                    button.setRequirements(reqStack[0])
                                }
                                view.appendButton(button);
                                break;
                            default:
                        }
                    }

                    // this.Views.push(view);
                    this.Views[xmlView.getAttribute('name')] = view;
                }
            }

            private parseXMLRequirements(item: Element, stack: Array<RequirementAND|RequirementOR>){
                for(let itemXML of item.children){
                    let lastItem = stack[stack.length-1];
                    switch (itemXML.tagName){
                        case 'requirement':
                            let requirement = new Requirement(this);
                            if(itemXML.hasAttribute('name')){
                                requirement.name = itemXML.getAttribute('name');
                            }
                            if(itemXML.hasAttribute('pattern')){
                                requirement.pattern = itemXML.getAttribute('pattern');
                            }
                            if(itemXML.hasAttribute('value')){
                                requirement.value = itemXML.getAttribute('value');
                            }
                            if(itemXML.hasAttribute('greater')){
                                requirement.greater = itemXML.getAttribute('greater');
                            }
                            if(itemXML.hasAttribute('less')){
                                requirement.less = itemXML.getAttribute('less');
                            }
                            lastItem.appendRequirement(requirement);
                            break
                        case 'and':
                            let requirementAND = new RequirementAND([]);
                            if(lastItem instanceof RequirementOR){
                                lastItem.appendRequirement(requirementAND);
                            }
                            stack.push(requirementAND);
                            this.parseXMLRequirements(itemXML, stack);
                            stack.pop();
                            break
                        case 'or':
                            let requirementOR = new RequirementOR([]);
                            if(lastItem instanceof RequirementAND){
                                lastItem.appendRequirement(requirementOR);
                            }
                            stack.push(requirementOR);
                            this.parseXMLRequirements(itemXML, stack);
                            stack.pop();
                            break
                    }
                }
            }

            show(){
                document.body.appendChild(this.element);

                if(typeof this.initView === "string"){
                    this.setView(this.initView);
                } else {
                    this.setView(Object.keys(this.Views)[0]);
                }
            }

            close(confirmation = true){
                if(document.body.contains(this.element)) {
                    if(confirmation && JSON.stringify(this.initValues) !== JSON.stringify(this.value())){
                        if(confirm('are you sure, you want to leave unsaved?')){
                            document.body.removeChild(this.element);
                        }
                    } else {
                        document.body.removeChild(this.element);
                    }
                }
            }
        }

        export class View {
            public Content: Array<ContentHTML|ContentForm> = [];
            public Context: Dialog;
            private view: HTMLDivElement;
            private name: string;
            buttons: Array<Button> = [];
            private buttonContainer: HTMLDivElement;
            private content: HTMLDivElement;

            constructor(context: Dialog) {
                this.Context = context;

                this.view = document.createElement('div');
                this.view.className = 'view';

                this.content = document.createElement('div');
                this.content.className = 'content-container';
                this.view.appendChild(this.content);

                this.buttonContainer = document.createElement('div');
                this.buttonContainer.className = 'button-container';
                this.view.appendChild(this.buttonContainer);
            }

            get(): HTMLDivElement{
                return this.view;
            }

            onShow(){
                for(let cont of this.Content){
                    if(cont instanceof ContentForm){
                        cont.onShow();
                    }
                }
            }

            // SETTER
            setName(name: string){
                this.name = name;
            }

            appendContent(conItem: ContentHTML|ContentForm){
                this.Content.push(conItem);
                this.content.appendChild(conItem.get())
            }

            appendButton(button: Button){
                this.buttons.push(button);
                this.buttonContainer.appendChild(button.get())
            }
        }

        class ContentHTML {
            private HTML: string
            private content: HTMLDivElement;
            constructor(definition: string) {
                this.HTML = definition;

                this.content = document.createElement('div');
                this.content.className = 'dialog-content html';
                this.content.innerHTML = this.HTML;
            }

            get(): HTMLDivElement{
                return this.content;
            }
        }

        class ContentForm {
            public Fields: Array<Field> = [];
            public form: HTMLDivElement;
            private Orientation: 'horizontal'|'vertical' = 'vertical';

            constructor() {
                this.form = document.createElement('div');
                this.form.className = 'dialog-content form';
            }

            get(): HTMLDivElement{
                return this.form;
            }

            onShow(){
                for(let field of this.Fields){
                    field.onShow();
                }
            }

            // SETTER
            setOrientation(orientation: 'horizontal'|'vertical'){
                this.Orientation = orientation;
                this.form.setAttribute('orientation', this.Orientation);
            }

            appendField(field: Field){
                this.Fields.push(field);
                this.form.appendChild(field.get());
            }
        }

        class Field {
            protected container: HTMLDivElement;
            public HTMLElements: Array<HTMLElement> = []

            protected context: Maknapp.Dialog.View;
            private Name: string;
            protected Hint: string;
            protected Requirements: RequirementAND;
            private visibility: boolean;
            protected required: boolean = false;
            private label: string;

            constructor(context: View) {
                this.context = context;
            }

            get(): HTMLDivElement{
                return document.createElement('div');
            }

            value() {
                return undefined
            }

            onChange(){
                this.context.Context.requirementsCheck();
            }

            onShow(){

            }

            // Set
            setName(name: string){
                this.context.Context.remove(this.Name);
                this.context.Context.set(name, this);
                this.Name = name;
            }

            setLabel(label: string){
                this.label = label;
                if(this.required){
                    this.HTMLElements['label'].innerText = label + '*';
                } else {
                    this.HTMLElements['label'].innerText = label;
                }
            }

            setVisibility(visibility: boolean){
                this.visibility = visibility;
                if(visibility){
                    this.container.style.display = '';
                } else {
                    this.container.style.display = 'none';
                }
            }

            setRequired(required: boolean){
                this.required = required;
                this.setLabel(this.label);
            }

            setHint(hint: string){
                if(this.HTMLElements['supportText'] instanceof HTMLDivElement){
                    this.Hint = hint;
                    this.HTMLElements['supportText'].innerText = this.Hint;
                }
            }

            setPrefix(prefix: string){
                this.HTMLElements['prefix'].innerText = prefix;
                if(prefix.trim() === ''){
                    this.HTMLElements['prefix'].style.display = 'none';
                } else {
                    this.HTMLElements['prefix'].style.display = '';
                }
            }

            setSuffix(suffix: string){
                this.HTMLElements['suffix'].innerText = suffix;
                if(suffix.trim() === ''){
                    this.HTMLElements['suffix'].style.display = 'none';
                } else {
                    this.HTMLElements['suffix'].style.display = '';
                }
            }

            setRequirements(requirements: RequirementAND){
                this.Requirements = requirements;
            }

            checkRequirements(){
                if(typeof this.Requirements !== "undefined"){
                    if(this.Requirements.check()){
                        this.setVisibility(true);
                    } else {
                        this.setVisibility(false);
                    }
                }
            }
        }

        export class FieldText extends Field{
            public Label: string;
            public pattern: string;
            public CheckHint: string;
            public MaxLength: number;

            private input: HTMLInputElement;

            constructor(context: View) {
                super(context);
                this.init();
            }

            init(){
                this.container = document.createElement('div');
                this.container.className = 'field-container';

                let inputContainer = document.createElement('div');
                inputContainer.className = 'input';
                this.container.appendChild(inputContainer);

                this.HTMLElements['prefix'] = document.createElement('div');
                this.HTMLElements['prefix'].className = 'field-prefix';
                inputContainer.appendChild(this.HTMLElements['prefix']);

                this.HTMLElements['input'] = document.createElement('input');
                this.HTMLElements['input'].type = 'text';
                inputContainer.appendChild(this.HTMLElements['input']);

                this.HTMLElements['suffix'] = document.createElement('div');
                this.HTMLElements['suffix'].className = 'field-suffix';
                inputContainer.appendChild(this.HTMLElements['suffix']);

                this.HTMLElements['label'] = document.createElement('div');
                this.HTMLElements['label'].className = 'field-label';
                this.HTMLElements['label'].innerText = this.Label;
                this.container.appendChild(this.HTMLElements['label']);

                this.HTMLElements['supportText'] = document.createElement('div');
                this.HTMLElements['supportText'].className = 'field-supportText';
                this.container.appendChild(this.HTMLElements['supportText']);
                if(typeof this.Hint === "string") {
                    this.HTMLElements['supportText'].innerText = this.Hint;
                }

                this.HTMLElements['count'] = document.createElement('div');
                this.HTMLElements['count'].className = 'field-count';
                this.container.appendChild(this.HTMLElements['count']);

                // Events
                this.container.addEventListener('click', function (event: MouseEvent){
                    if(event.target !== this.HTMLElements['input']){
                        this.HTMLElements['input'].focus()
                    }
                }.bind(this), false)
                this.HTMLElements['input'].addEventListener('focus', function(){
                    this.container.className = 'field-container focus';
                }.bind(this), false);
                this.HTMLElements['input'].addEventListener('focusout', this.inputCheck.bind(this), false);
                this.HTMLElements['input'].addEventListener('keyup', this.onChange.bind(this), false);
                this.HTMLElements['input'].addEventListener('keydown', this.onChange.bind(this), false);
            }

            get(): HTMLDivElement{
                return this.container;
            }

            check(): string|undefined {
                if(this.required && this.HTMLElements['input'].value === ''){
                    return ERROR_MSG_REQUIRED;
                }

                if(typeof this.pattern === "string" && this.HTMLElements['input'].value.length > 0){
                    const regex = new RegExp(this.pattern);
                    if(regex.test(this.HTMLElements['input'].value)){
                        return undefined;
                    } else {
                        if(typeof this.CheckHint === 'string'){
                            return this.CheckHint;
                        } else {
                            return 'Values does not match `' + this.pattern + '`';
                        }
                    }
                } else return undefined;
            }

            value(): string|undefined {
                if(this.HTMLElements['input'].value.length === 0) return undefined
                else return this.HTMLElements['input'].value;
            }

            // SETTER
            setValue(value: string) {
                this.HTMLElements['input'].value = value;
                this.inputCheck();
                this.onChange();
            }

            setPlaceholder(placeholder: string){
                this.HTMLElements['input'].placeholder = placeholder;
            }

            setMaxLength(maxLength: number){
                this.MaxLength = maxLength;
                this.HTMLElements['input'].maxLength = maxLength;

                this.onChange();
            }

            setCheck(check: string, hint: string){
                this.pattern = check;
                this.CheckHint = hint;
                this.inputCheck();
            }

            // Internal
            private inputCheck(){
                let classes = ['field-container']
                if(this.HTMLElements['input'].value !== ""){
                    classes.push('filled');
                }

                let check = this.check();
                if(typeof check !== 'undefined'){
                    classes.push('error');
                    this.HTMLElements['supportText'].innerText = check;
                } else if(typeof this.Hint === "string") {
                    this.HTMLElements['supportText'].innerText = this.Hint;
                }
                this.container.className = classes.join(' ');
            }

            onChange(){
                this.context.Context.requirementsCheck();
                if(typeof this.MaxLength === "number") {
                    this.HTMLElements['count'].innerText = this.HTMLElements['input'].value.length + " / " + this.MaxLength;
                }
            }
        }

        export class FieldNumber extends Field{
            public Label: string
            public Step: number;
            public Min: number;
            public Max: number;

            constructor(context: View) {
                super(context);
                this.init();
            }

            init(){
                this.container = document.createElement('div');
                this.container.className = 'field-container';

                let inputContainer = document.createElement('div');
                inputContainer.className = 'input';
                this.container.appendChild(inputContainer);

                this.HTMLElements['prefix'] = document.createElement('div');
                this.HTMLElements['prefix'].className = 'field-prefix';
                inputContainer.appendChild( this.HTMLElements['prefix']);

                this.HTMLElements['input'] = document.createElement('input');
                this.HTMLElements['input'].type = 'number';
                this.HTMLElements['input'].style.textAlign = 'right';
                inputContainer.appendChild(this.HTMLElements['input']);

                this.HTMLElements['suffix'] = document.createElement('div');
                this.HTMLElements['suffix'].className = 'field-suffix';
                inputContainer.appendChild(this.HTMLElements['suffix']);

                this.HTMLElements['label'] = document.createElement('div');
                this.HTMLElements['label'].className = 'field-label';
                this.HTMLElements['label'].innerText = this.Label;
                this.container.appendChild(this.HTMLElements['label']);

                this.HTMLElements['supportText'] = document.createElement('div');
                this.HTMLElements['supportText'].className = 'field-supportText';
                this.container.appendChild(this.HTMLElements['supportText']);

                // Events
                this.container.addEventListener('click', function (event: MouseEvent){
                    if(event.target !== this.HTMLElements['input']) this.HTMLElements['input'].focus();
                }.bind(this), false)
                this.HTMLElements['input'].addEventListener('focus', function(){
                    this.container.className = 'field-container focus';
                }.bind(this), false);
                this.HTMLElements['input'].addEventListener('focusout', this.inputCheck.bind(this), false);
                this.HTMLElements['input'].addEventListener('change', this.onChange.bind(this));
            }

            inputCheck(){
                let classes = ['field-container'];
                if(this.HTMLElements['input'].value !== ""){
                    classes.push('filled');
                }
                let check = this.check()
                if(typeof check !== "undefined"){
                    classes.push('error');
                    this.HTMLElements['supportText'].innerText = check;
                } else if(typeof this.Hint === "string") {
                    this.HTMLElements['supportText'].innerText = this.Hint;
                }
                this.container.className = classes.join(' ');
            }

            get(): HTMLDivElement{
                return this.container;
            }

            check(): string|undefined {
                // required
                if(this.required && this.HTMLElements['input'].value.length === 0){
                    return ERROR_MSG_REQUIRED;
                }

                // check min and max
                if(typeof this.Min === "number" && this.Min > Number(this.HTMLElements['input'].value)){
                    return "Value must be greater or equal than " + this.Min
                }
                if(typeof this.Max === "number" && this.Max < Number(this.HTMLElements['input'].value)){
                    return "Value must be lower or equal than " + this.Max
                }
                if(typeof this.Step === "number" && parseFloat((Number(this.HTMLElements['input'].value) / this.Step).toPrecision(12))%1 !== 0){
                    return "Value must be in step of " + this.Step
                }
                return undefined
            }

            value(): number|undefined {
                if(this.HTMLElements['input'].value.length === 0) return undefined
                else return Number(this.HTMLElements['input'].value);
            }

            // SETTER
            setValue(value: number) {
                this.HTMLElements['input'].value = value;
                this.inputCheck();
            }

            setPlaceholder(placeholder: string){
                this.HTMLElements['input'].placeholder = placeholder;
            }

            setMin(min: number){
                this.Min = min;
                this.HTMLElements['input'].min = String(min);
            }

            setMax(max: number){
                this.Max = max;
                this.HTMLElements['input'].max = String(max);
            }

            setStep(step: number){
                this.Step = step;
                this.HTMLElements['input'].step = String(step);
            }
        }

        export class FieldFile extends Field{
            public Label: string

            constructor(context: View) {
                super(context);
                this.init();
            }

            init(){
                this.container = document.createElement('div');
                this.container.className = 'field-container';

                let inputContainer = document.createElement('div');
                inputContainer.className = 'input';
                this.container.appendChild(inputContainer);

                this.HTMLElements['prefix'] = document.createElement('div');
                this.HTMLElements['prefix'].className = 'field-prefix';
                inputContainer.appendChild( this.HTMLElements['prefix']);

                this.HTMLElements['input'] = document.createElement('input');
                this.HTMLElements['input'].type = 'file';
                this.HTMLElements['input'].style.textAlign = 'right';
                inputContainer.appendChild(this.HTMLElements['input']);

                this.HTMLElements['suffix'] = document.createElement('div');
                this.HTMLElements['suffix'].className = 'field-suffix';
                inputContainer.appendChild(this.HTMLElements['suffix']);

                this.HTMLElements['label'] = document.createElement('div');
                this.HTMLElements['label'].className = 'field-label';
                this.HTMLElements['label'].innerText = this.Label;
                this.container.appendChild(this.HTMLElements['label']);

                this.HTMLElements['supportText'] = document.createElement('div');
                this.HTMLElements['supportText'].className = 'field-supportText';
                this.container.appendChild(this.HTMLElements['supportText']);

                // Events
                this.container.addEventListener('click', function (event: MouseEvent){
                    if(event.target !== this.HTMLElements['input']) this.HTMLElements['input'].focus();
                }.bind(this), false)
                this.HTMLElements['input'].addEventListener('focus', function(){
                    this.container.className = 'field-container focus';
                }.bind(this), false);
                this.HTMLElements['input'].addEventListener('focusout', this.inputCheck.bind(this), false);
                this.HTMLElements['input'].addEventListener('change', this.onChange.bind(this));
            }

            inputCheck(){
                let classes = ['field-container'];
                if(this.HTMLElements['input'].value !== ""){
                    classes.push('filled');
                }
                let check = this.check()
                if(typeof check !== "undefined"){
                    classes.push('error');
                    this.HTMLElements['supportText'].innerText = check;
                } else if(typeof this.Hint === "string") {
                    this.HTMLElements['supportText'].innerText = this.Hint;
                }
                this.container.className = classes.join(' ');
            }

            get(): HTMLDivElement{
                return this.container;
            }

            check(): string|undefined {
                // required
                if(this.required && this.HTMLElements['input'].value.length === 0){
                    return ERROR_MSG_REQUIRED;
                }

                return undefined
            }

            value(): number|undefined {
                if(this.HTMLElements['input'].value.length === 0) return undefined
                else return Number(this.HTMLElements['input'].value);
            }

            // SETTER
            setValue(value: number) {
                this.HTMLElements['input'].value = value;
                this.inputCheck();
            }

            setPlaceholder(placeholder: string){
                this.HTMLElements['input'].placeholder = placeholder;
            }
        }

        export class FieldDate extends Field{
            public Label: string;

            constructor(context: View) {
                super(context);
                this.init();
            }

            init(){
                this.container = document.createElement('div');
                this.container.className = 'field-container';

                let inputContainer = document.createElement('div');
                inputContainer.className = 'input';
                this.container.appendChild(inputContainer);

                this.HTMLElements['input'] = document.createElement('input');
                this.HTMLElements['input'].type = 'date';
                this.HTMLElements['input'].pattern = "\d{4}-\d{2}-\d{2}"
                inputContainer.appendChild(this.HTMLElements['input']);

                this.HTMLElements['label'] = document.createElement('div');
                this.HTMLElements['label'].className = 'field-label';
                this.HTMLElements['label'].innerText = this.Label;
                this.container.appendChild(this.HTMLElements['label']);

                this.HTMLElements['supportText'] = document.createElement('div');
                this.HTMLElements['supportText'].className = 'field-supportText';
                this.container.appendChild(this.HTMLElements['supportText']);

                // Events
                this.container.addEventListener('click', function (event: MouseEvent){
                    if(event.target !== this.HTMLElements['input']) this.HTMLElements['input'].focus();
                }.bind(this), false)
                this.HTMLElements['input'].addEventListener('focus', function(){
                    this.container.className = 'field-container focus';
                }.bind(this), false);
                this.HTMLElements['input'].addEventListener('focusout', this.inputCheck.bind(this), false);
                this.HTMLElements['input'].addEventListener('change', this.onChange.bind(this));
            }

            inputCheck() {
                let classes = ['field-container']
                if(this.HTMLElements['input'].value !== ""){
                    classes.push('filled');
                }
                let check = this.check()
                if(typeof check !== "undefined"){
                    classes.push('error');
                    this.HTMLElements['supportText'].innerText = check;
                } else if(typeof this.Hint === "string") {
                    this.HTMLElements['supportText'].innerText = this.Hint;
                }
                this.container.className = classes.join(' ');
            }

            get(): HTMLDivElement{
                return this.container;
            }

            check(): string|undefined {
                // required
                if(this.required && this.HTMLElements['input'].value.length === 0){
                    return ERROR_MSG_REQUIRED;
                }

                return undefined;
            }

            value(): string|undefined {
                if(this.HTMLElements['input'].value.length === 0) return undefined
                else return this.HTMLElements['input'].value;
            }

            // SETTER
            setValue(date: string){
                this.HTMLElements['input'].value = date;
                this.inputCheck();
            }
        }

        export class FieldTime extends Field{
            public Label: string;

            constructor(context: View) {
                super(context);
                this.init();
            }

            init(){
                this.container = document.createElement('div');
                this.container.className = 'field-container';

                let inputContainer = document.createElement('div');
                inputContainer.className = 'input';
                this.container.appendChild(inputContainer);

                this.HTMLElements['input'] = document.createElement('input');
                this.HTMLElements['input'].type = 'time';
                this.HTMLElements['input'].pattern = "\d{2}:\d{2}"
                inputContainer.appendChild(this.HTMLElements['input']);

                this.HTMLElements['label'] = document.createElement('div');
                this.HTMLElements['label'].className = 'field-label';
                this.HTMLElements['label'].innerText = this.Label;
                this.container.appendChild(this.HTMLElements['label']);

                this.HTMLElements['supportText'] = document.createElement('div');
                this.HTMLElements['supportText'].className = 'field-supportText';
                this.container.appendChild(this.HTMLElements['supportText']);

                // Events
                this.container.addEventListener('click', function (event: MouseEvent){
                    if(event.target !== this.HTMLElements['input']) this.HTMLElements['input'].focus();
                }.bind(this), false)
                this.HTMLElements['input'].addEventListener('focus', function(){
                    this.container.className = 'field-container focus';
                }.bind(this), false);
                this.HTMLElements['input'].addEventListener('focusout', this.inputCheck.bind(this), false);
                this.HTMLElements['input'].addEventListener('change', this.onChange.bind(this));
            }

            inputCheck() {
                let classes = ['field-container']
                if(this.HTMLElements['input'].value !== ""){
                    classes.push('filled');
                }
                let check = this.check()
                if(typeof check !== "undefined"){
                    classes.push('error');
                    this.HTMLElements['supportText'].innerText = check;
                } else if(typeof this.Hint === "string") {
                    this.HTMLElements['supportText'].innerText = this.Hint;
                }
                this.container.className = classes.join(' ');
            }

            get(): HTMLDivElement{
                return this.container;
            }

            check(): string|undefined {
                // required
                if(this.required && this.HTMLElements['input'].value.length === 0){
                    return ERROR_MSG_REQUIRED;
                }

                return undefined;
            }

            value(): string|undefined {
                if(this.HTMLElements['input'].value.length === 0) return undefined
                else return this.HTMLElements['input'].value;
            }

            // SETTER
            setValue(date: string){
                this.HTMLElements['input'].value = date;
                this.inputCheck();
            }
        }

        export class FieldDatetime extends Field{
            public Label: string;

            constructor(context: View) {
                super(context);
                this.init();
            }

            init(){
                this.container = document.createElement('div');
                this.container.className = 'field-container';

                let inputContainer = document.createElement('div');
                inputContainer.className = 'input';
                this.container.appendChild(inputContainer);

                this.HTMLElements['input'] = document.createElement('input');
                this.HTMLElements['input'].type = 'datetime-local';
                this.HTMLElements['input'].pattern = "[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}"
                inputContainer.appendChild(this.HTMLElements['input']);

                this.HTMLElements['label'] = document.createElement('div');
                this.HTMLElements['label'].className = 'field-label';
                this.HTMLElements['label'].innerText = this.Label;
                this.container.appendChild(this.HTMLElements['label']);

                this.HTMLElements['supportText'] = document.createElement('div');
                this.HTMLElements['supportText'].className = 'field-supportText';
                this.container.appendChild(this.HTMLElements['supportText']);

                // Events
                this.container.addEventListener('click', function (event: MouseEvent){
                    if(event.target !== this.HTMLElements['input']) this.HTMLElements['input'].focus();
                }.bind(this), false)
                this.HTMLElements['input'].addEventListener('focus', function(){
                    this.container.className = 'field-container focus';
                }.bind(this), false);
                this.HTMLElements['input'].addEventListener('focusout', this.inputCheck.bind(this), false);
                this.HTMLElements['input'].addEventListener('change', this.onChange.bind(this));
            }

            inputCheck() {
                let classes = ['field-container']
                if(this.HTMLElements['input'].value !== ""){
                    classes.push('filled');
                }
                let check = this.check()
                if(typeof check !== "undefined"){
                    classes.push('error');
                    this.HTMLElements['supportText'].innerText = check;
                } else if(typeof this.Hint === "string") {
                    this.HTMLElements['supportText'].innerText = this.Hint;
                }
                this.container.className = classes.join(' ');
            }

            get(): HTMLDivElement{
                return this.container;
            }

            check(): string|undefined {
                // required
                if(this.required && this.HTMLElements['input'].value.length === 0){
                    return ERROR_MSG_REQUIRED;
                }

                return undefined;
            }

            value(): string|undefined {
                if(this.HTMLElements['input'].value.length === 0) return undefined
                else return this.HTMLElements['input'].value;
            }

            // SETTER
            setValue(date: string){
                this.HTMLElements['input'].value = date;
                this.inputCheck();
            }
        }

        export class FieldSwitch extends Field{
            constructor(context: View) {
                super(context);

                this.init();
            }

            init(){
                this.container = document.createElement('div');
                this.container.className = 'field-container switch';

                // Label
                this.HTMLElements['label'] = document.createElement("div");
                this.HTMLElements['label'].className = "field-label";
                this.container.appendChild(this.HTMLElements['label']);

                this.HTMLElements['supportText'] = document.createElement('div');
                this.HTMLElements['supportText'].className = 'field-supportText';
                this.container.appendChild(this.HTMLElements['supportText']);

                // Switch
                let switchContainer = document.createElement("div");
                switchContainer.className = "field-switch-container";
                this.container.appendChild(switchContainer);

                this.HTMLElements['prefix'] = document.createElement('div');
                this.HTMLElements['prefix'].className = 'field-prefix';
                switchContainer.appendChild(this.HTMLElements['prefix']);

                let switchObj = document.createElement("label");
                switchContainer.appendChild(switchObj);

                this.HTMLElements['input'] = document.createElement("input");
                this.HTMLElements['input'].type = "checkbox";
                switchObj.appendChild(this.HTMLElements['input']);

                let slider = document.createElement("span");
                slider.className = "field-switch-slider";
                switchObj.appendChild(slider);

                this.HTMLElements['suffix'] = document.createElement('div');
                this.HTMLElements['suffix'].className = 'field-suffix';
                switchContainer.appendChild(this.HTMLElements['suffix']);

                // Events
                this.HTMLElements['input'].addEventListener('change', this.onChange.bind(this));
            }

            onChange(){
                super.onChange();
                let classes = ['field-container switch'];

                let check = this.check();
                if(typeof check !== "undefined"){
                    classes.push('error');
                    this.HTMLElements['supportText'].innerText = check;
                } else if(typeof this.Hint === "string") {
                    this.HTMLElements['supportText'].innerText = this.Hint;
                } else {
                    this.HTMLElements['supportText'].innerText = '';
                }
                this.container.className = classes.join(' ');
            }

            get(): HTMLDivElement{
                return this.container;
            }

            check(): string|undefined {
                // required
                if(this.required && !this.HTMLElements['input'].checked){
                    return ERROR_MSG_REQUIRED;
                }

                return undefined;
            }

            value(): boolean {
                return this.HTMLElements['input'].checked;
            }

            // SETTER
            setValue(value: boolean){
                this.HTMLElements['input'].checked = value;
            }
        }

        interface listItem {
            icon?: string
            iconStyle?: string
            headline: string
            supportText?: string
            value: string
        }

        type list = Array<listItem>;

        export class FieldSelect extends Field{
            public Label: string;
            public items: list = [];

            private input: HTMLInputElement;
            private selectedItem: listItem;
            private type: "static" | "search" = 'static';
            private source: string;

            constructor(context: View) {
                super(context);
                this.init();
            }

            init(){
                this.container = document.createElement('div');
                this.container.className = 'field-container select';

                let inputContainer = document.createElement('div');
                inputContainer.className = 'input';
                this.container.appendChild(inputContainer);

                this.HTMLElements['prefix'] = document.createElement('div');
                this.HTMLElements['prefix'].className = 'field-prefix';
                inputContainer.appendChild(this.HTMLElements['prefix']);

                this.HTMLElements['input'] = document.createElement('div');
                this.HTMLElements['input'].className = 'select';
                this.HTMLElements['input'].tabIndex = 0;
                inputContainer.appendChild(this.HTMLElements['input']);

                this.HTMLElements['value'] = document.createElement('div');
                this.HTMLElements['value'].className = 'value';
                this.HTMLElements['value'].innerText = 'select item';
                this.HTMLElements['input'].appendChild(this.HTMLElements['value']);

                this.HTMLElements['search'] = document.createElement('input');
                this.HTMLElements['search'].className = 'value';
                this.HTMLElements['search'].style.display = 'none';
                this.HTMLElements['input'].appendChild(this.HTMLElements['search']);

                this.HTMLElements['options'] = document.createElement('div');
                this.HTMLElements['options'].className = 'options';
                this.HTMLElements['input'].appendChild(this.HTMLElements['options']);

                this.HTMLElements['suffix'] = document.createElement('div');
                this.HTMLElements['suffix'].className = 'field-suffix';
                inputContainer.appendChild(this.HTMLElements['suffix']);

                this.HTMLElements['label'] = document.createElement('div');
                this.HTMLElements['label'].className = 'field-label';
                this.HTMLElements['label'].innerText = this.Label;
                this.container.appendChild(this.HTMLElements['label']);

                this.HTMLElements['supportText'] = document.createElement('div');
                this.HTMLElements['supportText'].className = 'field-supportText';
                this.container.appendChild(this.HTMLElements['supportText']);
                if(typeof this.Hint === "string") {
                    this.HTMLElements['supportText'].innerText = this.Hint;
                }

                this.HTMLElements['count'] = document.createElement('div');
                this.HTMLElements['count'].className = 'field-count';
                this.container.appendChild(this.HTMLElements['count']);

                // Events
                this.container.addEventListener('click', function (event: MouseEvent){
                    if(event.target !== this.HTMLElements['input']
                        && event.target !== this.HTMLElements['search']
                    ){
                        this.HTMLElements['input'].focus()
                    }
                }.bind(this), false)
                this.HTMLElements['input'].addEventListener('focus', function(){
                    this.container.className = 'field-container select focus';
                }.bind(this), false);
                this.HTMLElements['input'].addEventListener('focusout', this.inputCheck.bind(this), false);
                this.HTMLElements['search'].addEventListener('focus', function(){
                    this.container.className = 'field-container select focus';
                }.bind(this), false);
                this.HTMLElements['search'].addEventListener('focusout', this.inputCheck.bind(this), false);
                this.HTMLElements['search'].addEventListener('keyup', this.getSearch.bind(this));
            }

            get(): HTMLDivElement{
                return this.container;
            }

            check(): string|undefined {
                if(this.required && typeof this.selectedItem === 'undefined'){
                    return ERROR_MSG_REQUIRED;
                }

                return undefined;
            }

            value(): string|undefined {
                if(typeof this.selectedItem === 'undefined') return undefined
                else return this.selectedItem.value;
            }

            // SETTER
            setValue(value: string) {
                if(typeof this.items[value] === 'undefined'){
                    if(this.type === 'search'){
                        this.HTMLElements['search'].value = value;
                        this.getSearch(function(){
                            this.setValue(value)
                        }.bind(this))
                    } else {
                        console.error('select does not have the value ' + value);
                    }
                    return
                }

                this.selectedItem = this.items[value];
                if(this.type === 'static'){
                    while(this.HTMLElements['value'].firstChild) this.HTMLElements['value'].removeChild(this.HTMLElements['value'].firstChild);
                    this.HTMLElements['value'].appendChild(this.buildItem(this.items[value]));
                } else {
                    this.HTMLElements['search'].value = this.items[value].headline;
                }

                this.inputCheck();
                this.onChange();
            }

            setType(type: 'static' | 'search') {
                this.type = type;

                if(this.type === 'search'){
                    this.HTMLElements['search'].style.display = '';
                    this.HTMLElements['value'].style.display = 'none';
                } else {
                    this.HTMLElements['search'].style.display = 'none';
                    this.HTMLElements['value'].style.display = '';
                }
            }

            setSource(source: string) {
                this.source = source;
            }

            appendItem(item: listItem){
                this.items[item.value] = item;

                let option = this.buildItem(item);
                this.HTMLElements['options'].appendChild(option);

                option.addEventListener('click', function(event: PointerEvent){
                    event.preventDefault();
                    event.stopImmediatePropagation();
                    // @ts-ignore
                    document.activeElement.blur();

                    this.setValue(item.value);
                }.bind(this), false);
            }

            clearItems(){
                while(this.HTMLElements['options'].firstChild) this.HTMLElements['options'].removeChild(this.HTMLElements['options'].firstChild)
                this.items = [];
            }

            // Internal
            private inputCheck(){
                let classes = ['field-container select']
                if(typeof this.selectedItem !== "undefined"){
                    classes.push('filled');
                }

                let check = this.check();
                if(typeof check !== 'undefined'){
                    classes.push('error');
                    this.HTMLElements['supportText'].innerText = check;
                } else if(typeof this.Hint === "string") {
                    this.HTMLElements['supportText'].innerText = this.Hint;
                }
                this.container.className = classes.join(' ');
            }

            buildItem(definition: listItem): HTMLDivElement {
                let item = document.createElement('div');
                item.className = 'item';

                if(typeof definition.icon === 'string') {
                    let left = document.createElement('div');
                    left.className = 'flex-item';
                    item.appendChild(left);

                    let img = document.createElement('img');
                    img.src = definition.icon;
                    left.appendChild(img);

                    if(typeof definition.iconStyle === "string"){
                        img.className = definition.iconStyle;
                    }
                }

                let right = document.createElement('div');
                right.className = 'flex-item';
                item.appendChild(right);

                let headline = document.createElement('h4');
                headline.innerText = definition.headline;
                right.appendChild(headline);

                if(typeof definition.supportText === 'string') {
                    right.className = 'flex-item top';

                    let supportText = document.createElement('p');
                    supportText.innerText = definition.supportText;
                    right.appendChild(supportText);
                }

                return item;
            }

            private getSearch(callback = undefined){
                if(this.HTMLElements['search'].value.length >= 3){
                    fetch(this.source+'?q='+this.HTMLElements['search'].value)
                        .then((response) => response.json())
                        .then((data) => {
                            this.clearItems();
                            if(typeof data === 'object' && data !== null){
                                for(let item of data){
                                    this.appendItem(item);
                                }
                            }

                            if(typeof callback === 'function') callback();
                        });
                } else {
                    this.clearItems();
                }
            }
        }

        export class FieldCurrency extends Field {
            public Step: number;
            public Min: number;
            public Max: number;

            private Currencies: Array<string>;
            private currency: HTMLSelectElement;
            private InitCurrency: string;

            constructor(context: View) {
                super(context);
                this.init();

                this.setStep(0.01);
            }

            init(){
                this.container = document.createElement('div');
                this.container.className = 'field-container';

                let inputContainer = document.createElement('div');
                inputContainer.className = 'input';
                this.container.appendChild(inputContainer);

                this.HTMLElements['input'] = document.createElement('input');
                this.HTMLElements['input'].type = 'number';
                this.HTMLElements['input'].style.textAlign = 'right';
                inputContainer.appendChild(this.HTMLElements['input']);

                this.currency = document.createElement('select');
                inputContainer.appendChild(this.currency);

                this.HTMLElements['label'] = document.createElement('div');
                this.HTMLElements['label'].className = 'field-label';
                this.container.appendChild(this.HTMLElements['label']);

                this.HTMLElements['supportText'] = document.createElement('div');
                this.HTMLElements['supportText'].className = 'field-supportText';
                this.container.appendChild(this.HTMLElements['supportText']);

                // Events
                this.container.addEventListener('click', function (event: MouseEvent){
                    if(event.target !== this.HTMLElements['input'] && event.target !== this.currency){
                        this.HTMLElements['input'].focus()
                    }
                }.bind(this), false)
                this.HTMLElements['input'].addEventListener('focus', function(){
                    this.container.className = 'field-container focus';
                }.bind(this), false);
                this.HTMLElements['input'].addEventListener('focusout', this.inputCheck.bind(this), false);
                this.HTMLElements['input'].addEventListener('change', this.onChange.bind(this));
                this.currency.addEventListener('change', this.onChange.bind(this));
            }

            inputCheck(){
                let classes = ['field-container']
                if(this.HTMLElements['input'].value !== ""){
                    classes.push('filled');
                }
                let check = this.check()
                if(typeof check !== "undefined"){
                    classes.push('error');
                    this.HTMLElements['supportText'].innerText = check;
                } else if(typeof this.Hint === "string") {
                    this.HTMLElements['supportText'].innerText = this.Hint;
                } else {
                    this.HTMLElements['supportText'].innerText = ''
                }
                this.container.className = classes.join(' ');
            }

            get(): HTMLDivElement{
                return this.container;
            }

            check(): string|undefined {
                // required
                if(this.required && !this.HTMLElements['input'].checked){
                    return ERROR_MSG_REQUIRED;
                }

                // check min and max
                if(typeof this.Min === "number" && this.Min > Number(this.HTMLElements['input'].value)){
                    return "Value must be greater or equal than " + this.Min
                }
                if(typeof this.Max === "number" && this.Max < Number(this.HTMLElements['input'].value)){
                    return "Value must be lower or equal than " + this.Max
                }
                if(typeof this.Step === "number" && parseFloat((Number(this.HTMLElements['input'].value) / this.Step).toPrecision(12))%1 !== 0){
                    return "Value must be in step of " + this.Step
                }
                return undefined
            }

            value(): currency {
                return {
                    Value: Number(this.HTMLElements['input'].value),
                    Currency: this.currency.value
                };
            }

            // SETTER
            setValue(value: currency){
                this.HTMLElements['input'].value = value.Value;
                this.currency.value = value.Currency;
                this.inputCheck();
            }

            setPlaceholder(placeholder: string){
                this.HTMLElements['input'].placeholder = placeholder;
            }

            setMin(min: number){
                this.Min = min;
                this.HTMLElements['input'].min = String(min);
            }

            setMax(max: number){
                this.Max = max;
                this.HTMLElements['input'].max = String(max);
            }

            setStep(step: number){
                this.Step = step;
                this.HTMLElements['input'].step = String(step);
            }

            setCurrencies(currencies: Array<string>){
                this.Currencies = currencies;

                while(this.currency.firstChild) this.currency.removeChild(this.currency.firstChild);
                for(let cur of this.Currencies) {
                    let option = document.createElement('option');
                    option.innerText = cur;
                    this.currency.appendChild(option);

                    if(cur === this.InitCurrency){
                        option.selected = true;
                    }
                }
            }
        }

        export class FieldLocation extends Field {

            private optionMap: boolean = false;
            private map;
            private layer;
            private marker;

            constructor(context: View) {
                super(context);
                this.init();
            }

            init(){
                this.container = document.createElement('div');
                this.container.className = 'field-container location';

                let inputContainer = document.createElement('div');
                inputContainer.className = 'input';
                this.container.appendChild(inputContainer);

                this.HTMLElements['containerOptions'] =  document.createElement('div');
                this.HTMLElements['containerOptions'].className = 'item';
                this.HTMLElements['containerOptions'].style.display = 'none';
                inputContainer.appendChild(this.HTMLElements['containerOptions']);

                this.HTMLElements['toggle-map'] = document.createElement('button');
                this.HTMLElements['toggle-map'].innerText = 'show map';
                this.HTMLElements['containerOptions'].appendChild(this.HTMLElements['toggle-map']);

                // Number + N/S select
                let containerLatitude =  document.createElement('div');
                containerLatitude.className = 'item';
                inputContainer.appendChild(containerLatitude);

                this.HTMLElements['latitude-dd'] = document.createElement('input');
                this.HTMLElements['latitude-dd'].type = "number";
                this.HTMLElements['latitude-dd'].placeholder = 'Latitude';
                this.HTMLElements['latitude-dd'].min = '0';
                this.HTMLElements['latitude-dd'].max = '90';
                containerLatitude.appendChild(this.HTMLElements['latitude-dd']);

                this.HTMLElements['latitude-select'] = document.createElement('select');
                this.HTMLElements['latitude-select'].style.width = '45px';
                containerLatitude.appendChild(this.HTMLElements['latitude-select']);
                for(let opt of ['N', 'S']) {
                    let option = document.createElement('option');
                    option.innerText = opt;
                    this.HTMLElements['latitude-select'].appendChild(option);
                }

                // Number + E/W select
                let containerLongitude =  document.createElement('div');
                containerLongitude.className = 'item';
                inputContainer.appendChild(containerLongitude);

                this.HTMLElements['longitude-dd'] = document.createElement('input');
                this.HTMLElements['longitude-dd'].type = "number";
                this.HTMLElements['longitude-dd'].placeholder = 'Longitude';
                this.HTMLElements['longitude-dd'].min = '0';
                this.HTMLElements['longitude-dd'].max = '180';
                containerLongitude.appendChild(this.HTMLElements['longitude-dd']);

                this.HTMLElements['longitude-select'] = document.createElement('select');
                this.HTMLElements['longitude-select'].style.width = '45px';
                containerLongitude.appendChild(this.HTMLElements['longitude-select']);
                for(let opt of ['E', 'W']) {
                    let option = document.createElement('option');
                    option.innerText = opt;
                    this.HTMLElements['longitude-select'].appendChild(option);
                }

                this.HTMLElements['label'] = document.createElement('div');
                this.HTMLElements['label'].className = 'field-label';
                this.container.appendChild(this.HTMLElements['label']);

                this.HTMLElements['supportText'] = document.createElement('div');
                this.HTMLElements['supportText'].className = 'field-supportText';
                this.container.appendChild(this.HTMLElements['supportText']);

                // Map
                this.HTMLElements['mapContainer'] = document.createElement('div');
                this.HTMLElements['mapContainer'].style.display = 'none';
                this.container.appendChild(this.HTMLElements['mapContainer']);

                let mapOptions = {
                    center: [53.52, 10],
                    scrollWheelZoom: false,
                    zoom: 10,
                };
                // @ts-ignore
                this.map = new L.map(this.HTMLElements['mapContainer'], mapOptions);
                // @ts-ignore
                this.layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
                this.map.addLayer(this.layer);

                this.map.on('click', function(event){
                    this.setValue({
                        Latitude: event.latlng.lat,
                        Longitude: event.latlng.lng
                    })
                    this.inputCheck();
                    if(this.map) this.updateMarkerPosition();
                }.bind(this));

                // Events
                this.container.addEventListener('click', function (event: MouseEvent){
                    if(event.target === this.container){
                        this.HTMLElements['latitude-dd'].focus()
                    } else if(event.target === this.HTMLElements['latitude-select']
                        || event.target == this.HTMLElements['longitude-select']
                    ) {
                        this.inputCheck();
                    }
                }.bind(this), false);
                this.HTMLElements['latitude-dd'].addEventListener('focus', function(){
                    this.container.className = 'field-container location focus';
                }.bind(this), false);
                this.HTMLElements['longitude-dd'].addEventListener('focus', function(){
                    this.container.className = 'field-container location focus';
                }.bind(this), false);
                this.HTMLElements['latitude-dd'].addEventListener('focusout', this.inputCheck.bind(this), false);
                this.HTMLElements['longitude-dd'].addEventListener('focusout', this.inputCheck.bind(this), false);
                this.HTMLElements['latitude-select'].addEventListener('focusout', this.inputCheck.bind(this), false);
                this.HTMLElements['longitude-select'].addEventListener('focusout', this.inputCheck.bind(this), false);
                this.HTMLElements['latitude-dd'].addEventListener('change', this.onChange.bind(this));
                this.HTMLElements['longitude-dd'].addEventListener('change', this.onChange.bind(this));
                this.HTMLElements['latitude-select'].addEventListener('change', this.onChange.bind(this));
                this.HTMLElements['longitude-select'].addEventListener('change', this.onChange.bind(this));
                this.HTMLElements['toggle-map'].addEventListener('click', function(){
                    if(this.HTMLElements['toggle-map'].innerText === 'show map'){
                        this.HTMLElements['toggle-map'].innerText = 'hide map';
                        this.HTMLElements['mapContainer'].style.display = '';
                        this.onShow();
                        this.inputCheck();
                    } else {
                        this.HTMLElements['toggle-map'].innerText = 'show map';
                        this.HTMLElements['mapContainer'].style.display = 'none';
                    }
                }.bind(this), false);
            }

            onChange(){
                super.onChange();
                if(this.map) this.updateMarkerPosition();
            }

            onShow(){
                this.map.invalidateSize();
            }

            inputCheck(){
                let classes = ['field-container', 'location'];
                if(this.HTMLElements['latitude-dd'].value !== ''
                    || this.HTMLElements['longitude-dd'].value !== ''
                    || this.HTMLElements['latitude-select'] === document.activeElement
                    || this.HTMLElements['longitude-select'] === document.activeElement
                    || this.HTMLElements['toggle-map'].innerText === 'hide map'
                ){
                    classes.push('filled');
                }
                let check = this.check()
                if(typeof check !== "undefined"){
                    classes.push('error');
                    this.HTMLElements['supportText'].innerText = check;
                } else if(typeof this.Hint === "string") {
                    this.HTMLElements['supportText'].innerText = this.Hint;
                } else {
                    this.HTMLElements['supportText'].innerText = ''
                }
                this.container.className = classes.join(' ');
            }

            get(): HTMLDivElement{
                setTimeout(function(){
                    this.map.invalidateSize();
                }.bind(this), 200);
                return this.container;
            }

            check(): string|undefined {
                // required
                if(this.required && (this.HTMLElements['latitude-dd'].value === '' || this.HTMLElements['longitude-dd'].value === '')){
                    return ERROR_MSG_REQUIRED;
                }

                if(Number(this.HTMLElements['latitude-dd'].value) < 0 || Number(this.HTMLElements['latitude-dd'].value) > 90){
                    return "The Latitude must be between 0° & 90°";
                }
                if(this.HTMLElements['longitude-dd'].value < 0 || this.HTMLElements['longitude-dd'].value > 180){
                    return "The Longitude must be between 0° & 180°";
                }
                return undefined
            }

            value(): location|undefined {
                if(typeof this.check() === 'undefined'
                    && this.HTMLElements['latitude-dd'].value !== ''
                    && this.HTMLElements['longitude-dd'].value !== ''
                ){
                    return {
                        Latitude: Number(this.HTMLElements['latitude-dd'].value) * (Number(this.HTMLElements['latitude-select'].value === 'N') * 2 - 1),
                        Longitude: Number(this.HTMLElements['longitude-dd'].value) * (Number(this.HTMLElements['longitude-select'].value === 'E') * 2 - 1),
                    }
                }
                return undefined;
            }

            // SETTER
            setValue(value: location){
                this.HTMLElements['latitude-dd'].value = Math.abs(value.Latitude);
                this.HTMLElements['longitude-dd'].value = Math.abs(value.Longitude);

                this.HTMLElements['latitude-select'].value = (value.Latitude >= 0) ? 'N' : 'S';
                this.HTMLElements['longitude-select'].value = (value.Longitude >= 0) ? 'E' : 'W';

                this.inputCheck();
                this.updateMarkerPosition();
            }

            setMap(map: boolean){
                this.optionMap = map;
                if(this.optionMap) {
                    this.HTMLElements['containerOptions'].style.display = '';
                } else {
                    this.HTMLElements['containerOptions'].style.display = 'none';
                }
            }

            updateMarkerPosition(){
                let position = [
                    Number(this.HTMLElements['latitude-dd'].value) * (Number(this.HTMLElements['latitude-select'].value === 'N') * 2 - 1),
                    Number(this.HTMLElements['longitude-dd'].value) * (Number(this.HTMLElements['longitude-select'].value === 'E') * 2 - 1)
                ];

                if(typeof this.marker === 'undefined'){
                    // @ts-ignore
                    this.marker = L.marker([0, 0]).addTo(this.map);
                }

                this.marker.setLatLng(position);
                this.map.panTo(position);
            }
        }

        interface address {
            streetNumber?: address_field_config,
            houseName?: address_field_config,
            streetNumberSuffix?: address_field_config,
            streetName?: address_field_config,
            streetType?: address_field_config,
            addressTypeIdentifier?: address_field_config,
            localMuniciplity?: address_field_config
            city?: address_field_config,
            governingDistrict?: address_field_config,
            postalArea?: address_field_config,
        }
        interface address_definition {
            [index: string]: {
                [index: string]: address
            }
        }
        interface address_field_config {
            label?: string,
            check?: string,
            checkHint?: string,
        }

        const ADDRESS_DEFINITION: address_definition = {
            "Austria": {
                "default": {
                    streetNumber: {label: "Number"},
                    streetNumberSuffix: {label: "Suffix"},
                    streetName: {label: "Street"},
                    city: {label: "City"},
                    postalArea: {label: "Postal Area", check: "^\\d{4}$", checkHint: "4 digits"},
                },
            },
            "Belgium": {
                "default": {
                    streetNumber: {label: "Number"},
                    streetNumberSuffix: {label: "Suffix"},
                    streetName: {label: "Street"},
                    city: {label: "City"},
                    postalArea: {label: "Postal Area", check: "^\\d{4}$", checkHint: "4 digits"},
                },
            },
            "Czech Republic": {
                "default": {
                    streetNumber: {label: "Number"},
                    streetNumberSuffix: {label: "Suffix"},
                    streetName: {label: "Street"},
                    city: {label: "City"},
                    postalArea: {label: "Postal Area", check: "^\\d{3}\\s\\d{2}$", checkHint: "e.g 115 03"},
                },
            },
            "Denmark": {
                "default": {
                    streetNumber: {label: "Number"},
                    streetNumberSuffix: {label: "Additional"},
                    streetName: {label: "Street"},
                    city: {label: "City"},
                    postalArea: {label: "Postal Code", check: "^\\d{4}$", checkHint: "4 digits"},
                },
            },
            "Germany": {
                "default": {
                    streetNumber: {label: "Number"},
                    streetNumberSuffix: {label: "Suffix"},
                    streetName: {label: "Street"},
                    city: {label: "City"},
                    postalArea: {label: "Postal Code", check: "^\\d{5}$", checkHint: "5 digits"},
                },
                "PO-BOX": {
                    addressTypeIdentifier: {label: "Post box"},
                    city: {label: "City"},
                    postalArea: {label: "Postal Code", check: "^\\d{5}$", checkHint: "5 digits"},
                },
            },
            "Estonia": {
                "default": {
                    streetNumber: {label: "Number"},
                    streetNumberSuffix: {label: "Additional"},
                    streetName: {label: "Street"},
                    city: {label: "City"},
                    postalArea: {label: "Postal Code", check: "^\\d{5}$", checkHint: "5 digits"},
                },
            },
            "Finland": {
                "default": {
                    streetNumber: {label: "Number"},
                    streetNumberSuffix: {label: "Additional"},
                    streetName: {label: "Street"},
                    city: {label: "City"},
                    postalArea: {label: "Postal Code", check: "^\\d{5}$", checkHint: "5 digits"},
                },
            },
            "France": {
                "default": {
                    streetNumber: {label: "Number"},
                    streetNumberSuffix: {label: "Additional"},
                    streetName: {label: "Street"},
                    city: {label: "City"},
                    postalArea: {label: "Postal Code", check: "^\\d{5}$", checkHint: "5 digits"},
                },
            },
            "Iceland": {
                "default": {
                    streetNumber: {label: "Number"},
                    streetNumberSuffix: {label: "Suffix"},
                    streetName: {label: "Street"},
                    city: {label: "City"},
                    postalArea: {label: "Postal Area", check: "^\\d{3}$", checkHint: "3 digits"},
                },
                "PO-BOX": {
                    addressTypeIdentifier: {label: "Post box"},
                    city: {label: "City"},
                    postalArea: {label: "Postal Area", check: "^\\d{3}$", checkHint: "3 digits"},
                },
            },
            "Ireland": {
                "default": {
                    streetNumber: {label: "Number"},
                    streetNumberSuffix: {label: "Additional"},
                    streetName: {label: "Street"},
                    city: {label: "City"},
                    postalArea: {label: "Postal Code", check: "^[A-Z0-9]{3}\\s[A-Z0-9]{3}$", checkHint: "e. g. A65 F4E2"},
                },
            },
            "Italy": {
                "default": {
                    streetNumber: {label: "Number"},
                    streetNumberSuffix: {label: "Suffix"},
                    streetName: {label: "Street"},
                    city: {label: "City"},
                    governingDistrict: {label: "Provice Code", check: "^[A-Z]{2}$", checkHint: "2 uppercase letters"},
                    postalArea: {label: "Postal Code", check: "^\\d{5}$", checkHint: "5 digits"},
                },
                "PO-BOX": {
                    addressTypeIdentifier: {label: "Post box"},
                    city: {label: "City"},
                    governingDistrict: {label: "Provice Code", check: "^[A-Z]{2}$", checkHint: "2 uppercase letters"},
                    postalArea: {label: "Postal Code", check: "^\\d{5}$", checkHint: "5 digits"},
                },
                "kilometric civic number": {
                    addressTypeIdentifier: {label: "Kilometer", check: "^\\d+$", checkHint: "only digits"},
                    city: {label: "City"},
                    governingDistrict: {label: "Provice Code", check: "^[A-Z]{2}$", checkHint: "2 uppercase letters"},
                    postalArea: {label: "Postal Code", check: "^\\d{5}$", checkHint: "5 digits"},
                },
            },
            "Luxembourg": {
                "default": {
                    streetNumber: {label: "Number"},
                    streetNumberSuffix: {label: "Additional"},
                    streetName: {label: "Street"},
                    city: {label: "City"},
                    postalArea: {label: "Postal Code", check: "^\\d{4}$", checkHint: "4 digits"},
                },
                "PO-BOX": {
                    addressTypeIdentifier: {label: "Post box"},
                    city: {label: "City"},
                    postalArea: {label: "Postal Code", check: "^\\d{4}$", checkHint: "4 digits"},
                },
            },
            "Netherlands": {
                "default": {
                    streetNumber: {label: "Number"},
                    streetNumberSuffix: {label: "Suffix"},
                    streetName: {label: "Street"},
                    city: {label: "City"},
                    postalArea: {label: "Postal Code", check: "^\\d{4}$", checkHint: "4 digits"},
                    governingDistrict: {label: "Region Code", check: "^[A-Z]{2}$", checkHint: "2 uppercase letters"},
                },
                "PO-BOX": {
                    addressTypeIdentifier: {label: "Post box"},
                    city: {label: "City"},
                    postalArea: {label: "Postal Code", check: "^\\d{5}$", checkHint: "5 digits"},
                    governingDistrict: {label: "Region Code", check: "^[A-Z]{2}$", checkHint: "2 uppercase letters"},
                },
                "business-reply": {
                    addressTypeIdentifier: {label: "Post box"},
                    city: {label: "City"},
                    postalArea: {label: "Postal Code", check: "^\\d{5}$", checkHint: "5 digits"},
                    governingDistrict: {label: "Region Code", check: "^[A-Z]{2}$", checkHint: "2 uppercase letters"},
                },
            },
            "Norway": {
                "default": {
                    streetNumber: {label: "Number"},
                    streetNumberSuffix: {label: "Additional"},
                    streetName: {label: "Street"},
                    city: {label: "City"},
                    postalArea: {label: "Postal Code", check: "^\\d{4}$", checkHint: "4 digits"},
                },
                "PO-BOX": {
                    addressTypeIdentifier: {label: "Post box"},
                    city: {label: "City"},
                    postalArea: {label: "Postal Code", check: "^\\d{4}$", checkHint: "4 digits"},
                },
            },
            "Poland": {
                "default": {
                    streetNumber: {label: "Number"},
                    streetNumberSuffix: {label: "Suffix"},
                    streetName: {label: "Street"},
                    city: {label: "City"},
                    postalArea: {label: "Postal Area", check: "^\\d{2}-\\d{3}$", checkHint: "e.g. 00-940"},
                },
            },
            "Portugal": {
                "default": {
                    streetNumber: {label: "Number"},
                    streetNumberSuffix: {label: "Suffix"},
                    streetName: {label: "Street"},
                    city: {label: "City"},
                    postalArea: {label: "Postal Area", check: "^\\d{4}-\\d{3}$", checkHint: "e.g. 1167-921"},
                },
            },
            "Spain": {
                "default": {
                    streetNumber: {label: "Number"},
                    streetNumberSuffix: {label: "Suffix"},
                    streetName: {label: "Street"},
                    city: {label: "City"},
                    postalArea: {label: "Postal Code", check: "^\\d{5}$", checkHint: "5 digits"},
                },
            },
            "Sweden": {
                "default": {
                    streetNumber: {label: "Number"},
                    streetNumberSuffix: {label: "Additional"},
                    streetName: {label: "Street"},
                    city: {label: "City"},
                    postalArea: {label: "Postal Code", check: "^\\d{3}\\s\\d{2}$", checkHint: "e.g. 113 49"},
                },
            },
            "Switzerland": {
                "default": {
                    streetNumber: {label: "Number"},
                    streetNumberSuffix: {label: "Suffix"},
                    streetName: {label: "Street"},
                    city: {label: "City"},
                    postalArea: {label: "Postal Code", check: "^\\d{4}$", checkHint: "4 digits"},
                },
                "PO-BOX": {
                    addressTypeIdentifier: {label: "Post box"},
                    city: {label: "City"},
                    postalArea: {label: "Postal Code", check: "^\\d{4}$", checkHint: "4 digits"},
                },
            },
            "United Kingdom": {
                "default": {
                    streetNumber: {label: "Number"},
                    streetNumberSuffix: {label: "Suffix"},
                    streetName: {label: "Street"},
                    city: {label: "City"},
                    postalArea: {label: "Postal Code", check: "^[A-Z0-9]{4}\\s[A-Z0-9]{3}$", checkHint: "e. g. HP19 3EQ"},
                },
            },
        }

        export class FieldAddress extends Field{
            private subFields: Array<Field> = [];

            constructor(context: View) {
                super(context);

                this.init();
            }

            init(){
                this.container = document.createElement('div');
                this.container.className = 'field-address';

                this.HTMLElements['label'] = document.createElement('h4');
                this.HTMLElements['label'] .className = 'label';
                this.HTMLElements['label'].innerText = 'Address';
                this.container.appendChild(this.HTMLElements['label']);

                this.subFields['country'] = new FieldSelect(this.context);
                this.subFields['country'].setLabel('Country');
                this.container.appendChild(this.subFields['country'].get());
                for(let country in ADDRESS_DEFINITION){
                    this.subFields['country'].appendItem({
                        headline: country,
                        value: country
                    })
                }

                this.subFields['type'] = new FieldSelect(this.context);
                this.subFields['type'].setLabel('Address Type');
                this.container.appendChild(this.subFields['type'].get());

                this.subFields['streetName'] = new FieldText(this.context);
                this.container.appendChild(this.subFields['streetName'].get());
                this.subFields['streetName'].setMaxLength(100);
                this.subFields['streetName'].setLabel('Street');

                this.subFields['houseName'] = new FieldText(this.context);
                this.container.appendChild(this.subFields['houseName'].get());
                this.subFields['houseName'].setMaxLength(100);
                this.subFields['houseName'].setLabel('House Name');

                this.subFields['streetNumber'] = new FieldNumber(this.context);
                this.container.appendChild(this.subFields['streetNumber'].get());
                this.subFields['streetNumber'].setLabel('Street Number');
                this.subFields['streetNumber'].get().style.gridColumn = 'span 2';

                this.subFields['streetNumberSuffix'] = new FieldText(this.context);
                this.container.appendChild(this.subFields['streetNumberSuffix'].get());
                this.subFields['streetNumberSuffix'].setMaxLength(16);
                this.subFields['streetNumberSuffix'].setLabel('Number Suffix');
                this.subFields['streetNumberSuffix'].get().style.gridColumn = 'span 2';

                this.subFields['streetType'] = new FieldText(this.context);
                this.container.appendChild(this.subFields['streetType'].get());
                this.subFields['streetType'].setMaxLength(64);
                this.subFields['streetType'].setLabel('Street Type');

                this.subFields['addressTypeIdentifier'] = new FieldText(this.context);
                this.container.appendChild(this.subFields['addressTypeIdentifier'].get());
                this.subFields['addressTypeIdentifier'].setMaxLength(100);
                this.subFields['addressTypeIdentifier'].setLabel('Address Type Identifier');

                this.subFields['localMuniciplity'] = new FieldText(this.context);
                this.container.appendChild(this.subFields['localMuniciplity'].get());
                this.subFields['localMuniciplity'].setMaxLength(100);
                this.subFields['localMuniciplity'].setLabel('Local Municiplity');

                this.subFields['city'] = new FieldText(this.context);
                this.container.appendChild(this.subFields['city'].get());
                this.subFields['city'].setMaxLength(100);
                this.subFields['city'].setLabel('City');

                this.subFields['governingDistrict'] = new FieldText(this.context);
                this.container.appendChild(this.subFields['governingDistrict'].get());
                this.subFields['governingDistrict'].setMaxLength(100);
                this.subFields['governingDistrict'].setLabel('Governing District');

                this.subFields['postalArea'] = new FieldText(this.context);
                this.container.appendChild(this.subFields['postalArea'].get());
                this.subFields['postalArea'].setMaxLength(100);
                this.subFields['postalArea'].setLabel('Postal Area');

                // Events
                this.subFields['country'].onChange = function(){
                    this.onDefinitionChange(true)
                }.bind(this)
                this.subFields['type'].onChange = function(){
                    this.onDefinitionChange(false);
                }.bind(this)

                // Init
                this.subFields['country'].setValue('Germany');
                this.onDefinitionChange();
            }

            onDefinitionChange(addressChange: boolean = true){

                // Set Type
                if(addressChange) {
                    this.subFields['type'].clearItems();
                    for(let type in ADDRESS_DEFINITION[this.subFields['country'].value()]){
                        this.subFields['type'].appendItem({
                            headline: type,
                            value: type
                        })
                    }
                    this.subFields['type'].setValue('default');
                }

                let address = ADDRESS_DEFINITION[this.subFields['country'].value()][this.subFields['type'].value()];

                // Fields
                for(let key of ['streetName', 'houseName', 'streetNumber', 'streetType', 'streetNumberSuffix', 'addressTypeIdentifier', 'localMuniciplity', 'city', 'governingDistrict', 'postalArea']){
                    if(this.subFields[key] instanceof Field){
                        if(typeof address[key] === 'object'){
                            this.subFields[key].get().style.display = '';

                            // Set Label
                            if(typeof address[key].label === "string") {
                                this.subFields[key].setLabel(address[key].label)
                            } else {
                                this.subFields[key].setLabel(key)
                            }

                            // Set Check
                            if(typeof address[key].check === "string") {
                                if(this.subFields[key] instanceof FieldText){
                                    this.subFields[key].setCheck(address[key].check, address[key].checkHint);
                                } else {
                                    this.subFields[key].setCheck(undefined, undefined);
                                }
                            }
                        } else {
                            this.subFields[key].get().style.display = 'none';
                        }
                    }
                }
            }

            get(): HTMLDivElement{
                return this.container;
            }

            value() {
                let address = {};
                for(let name in this.subFields){
                    let value = this.subFields[name].value();
                    if(typeof value !== 'undefined'){
                        address[name] = value;
                    }
                }

                return address;
            }

            // SETTER
            setStreetNumber(streetNumber: number){
                this.subFields['streetNumber'].setValue(streetNumber);
            }

            setHouseName(houseName: string){
                this.subFields['houseName'].setValue(houseName);
            }

            setStreetNumberSuffix(streetNumberSuffix: string){
                this.subFields['streetNumberSuffix'].setValue(streetNumberSuffix);
            }

            setStreetName(streetName: string){
                this.subFields['streetName'].setValue(streetName);
            }

            setStreetType(streetType: string){
                this.subFields['streetType'].setValue(streetType);
            }

            setStreetDirection(streetDirection: string){
                this.subFields['streetDirection'].setValue(streetDirection);
            }

            setAddressType(addressType: string){
                this.subFields['type'].setValue(addressType);
            }

            setAddressTypeIdentifier(addressTypeIdentifier: string){
                this.subFields['addressTypeIdentifier'].setValue(addressTypeIdentifier);
            }

            setLocalMuniciplity(localMuniciplity: string){
                this.subFields['localMuniciplity'].setValue(localMuniciplity);
            }

            setCity(city: string){
                this.subFields['city'].setValue(city);
            }

            setGoverningDistrict(governingDistrict: string){
                this.subFields['governingDistrict'].setValue(governingDistrict);
            }

            setPostalArea(postalArea: string){
                this.subFields['postalArea'].setValue(postalArea);
            }

            setCountry(country: string){
                this.subFields['country'].setValue(country);
            }
        }

        export class FieldFileSelector extends Field {
            private source: string;
            private selected: string;
            private selectedItem: HTMLDivElement;

            constructor(context: View) {
                super(context);

                this.init();
            }

            init(){
                this.container = document.createElement('div');
                this.container.className = 'field-container fileSelect filled';

                this.HTMLElements['label'] = document.createElement('div');
                this.HTMLElements['label'] .className = 'field-label';
                this.container.appendChild(this.HTMLElements['label']);

                this.HTMLElements['file-container'] = document.createElement('div');
                this.HTMLElements['file-container'].className = 'file-container';
                this.container.appendChild(this.HTMLElements['file-container']);
            }

            get(): HTMLDivElement{
                return this.container;
            }

            value() {
                return this.selected;
            }

            setSource(source: string) {
                this.source = source;
                this.getFiles();
            }

            setValue(value: string){
                this.selected = value;
            }

            private getFiles(callback = undefined){
                fetch(this.source)
                    .then((response) => response.json())
                    .then((data) => {
                        if(typeof data === 'object' && data !== null){
                            for(let item of data){
                                let file = document.createElement('div');
                                file.className = 'file';
                                file.innerText = item.title;
                                this.HTMLElements['file-container'].appendChild(file);

                                if(this.selected === item.value){
                                    file.className = 'file selected';
                                    this.selectedItem = file;
                                }

                                file.addEventListener('click', function(){
                                    this.selected = item.value;
                                    if(this.selectedItem instanceof HTMLDivElement) this.selectedItem.className = 'file';
                                    file.className = 'file selected';
                                    this.selectedItem = file;
                                }.bind(this))
                            }
                        }

                        if(typeof callback === 'function') callback();
                    });
            }
        }

        interface buttonAction {
            context: string,
            value: string,
        }

        interface dialogResponse{
            status: number
            location?: string
            errorMsg?: string
        }

        export class Button{
            protected requirements: RequirementAND;
            protected action: buttonAction;

            private element: HTMLButtonElement;
            private context: Maknapp.Dialog.Dialog;
            private visibility: boolean;

            constructor(context: Dialog) {
                this.context = context;
                this.element = document.createElement('button');

                this.element.addEventListener('click', this.onClick.bind(this));
            }

            get(): HTMLButtonElement {
                return this.element;
            }

            onClick() {
                switch(this.action.context) {
                    case 'dialog':
                        if(this.action.value.substring(0,4) === 'view'){
                            this.context.setView(this.action.value.substring(5));
                        } else if(this.action.value === 'back') {
                            this.context.setView(this.context.viewHistory.pop(), false);
                        }
                        break
                    case 'submit':
                        fetch(this.action.value, {
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/json'
                            },
                            body: JSON.stringify(this.context.value())
                        }).then((response) => response.json()).then((response: dialogResponse) => {
                            switch (response.status){
                                case 200:
                                    this.context.close(false);
                                    break
                                case 302:
                                    if(typeof response.location === 'string'){
                                        window.location.href = response.location;
                                    } else {
                                        console.error('status 300 need location for redirect')
                                    }
                                    break
                                default:
                                    let msg = `Error ${response.status}`;
                                    if(typeof response.errorMsg === 'string'){
                                        msg += ': ' + response.errorMsg;
                                    }
                                    this.context.showError(msg)
                                    console.error(response);
                            }
                        }).catch(error => {
                            this.context.showError(`Error: ${error}`);
                        });
                        break
                }
            }

            setLabel(label: string){
                this.element.innerText = label;
            }

            setAction(action: string){
                let parts = action.split(':');
                this.action = {
                    context: parts[0],
                    value: parts[1],
                }
            }

            setVisibility(visibility: boolean){
                this.visibility = visibility;
                if(visibility){
                    this.element.style.display = '';
                } else {
                    this.element.style.display = 'none';
                }
            }

            setRequirements(requirements: RequirementAND){
                this.requirements = requirements;
            }

            checkRequirements(){
                if(typeof this.requirements !== "undefined"){
                    if(this.requirements.check()){
                        this.setVisibility(true);
                    } else {
                        this.setVisibility(false);
                    }
                }
            }
        }
    }
}