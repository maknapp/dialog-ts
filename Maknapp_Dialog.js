var Maknapp;
(function (Maknapp) {
    let Dialog;
    (function (Dialog_1) {
        const ERROR_MSG_REQUIRED = 'This field is required';
        class Requirement {
            constructor(dialog) {
                this.dialog = dialog;
            }
            check() {
                let field = this.dialog.get(this.name);
                if (typeof field === "undefined") {
                    console.error('dialog check can not find field ' + this.name);
                    return true;
                }
                let value = field.value();
                if (field instanceof FieldText) {
                    let check = true;
                    if (typeof this.value !== 'undefined') {
                        check = value === this.value;
                    }
                    if (check && typeof this.pattern !== 'undefined') {
                        let regex = new RegExp(this.pattern);
                        check = regex.test(value);
                    }
                    return check;
                }
                else if (field instanceof FieldSwitch) {
                    return value.toString() === this.value;
                }
                else if (field instanceof FieldNumber) {
                    let check = true;
                    if (typeof this.value !== 'undefined') {
                        check = value === this.value;
                    }
                    if (check && typeof this.pattern !== 'undefined') {
                        let regex = new RegExp(this.pattern);
                        check = regex.test(value);
                    }
                    if (check && typeof this.greater !== 'undefined') {
                        check = value > this.greater;
                    }
                    if (check && typeof this.less !== 'undefined') {
                        check = value < this.less;
                    }
                    return check;
                }
                else if (field instanceof FieldSelect) {
                    return field.value() === this.value;
                }
                return true;
            }
        }
        class RequirementAND {
            constructor(items) {
                this.requirements = items;
            }
            check() {
                if (this.requirements.length === 0) {
                    return true;
                }
                else {
                    for (let req of this.requirements) {
                        if (!req.check())
                            return false;
                    }
                    return true;
                }
            }
            appendRequirement(item) {
                this.requirements.push(item);
            }
        }
        class RequirementOR {
            constructor(items) {
                this.requirements = items;
            }
            check() {
                if (this.requirements.length === 0) {
                    return true;
                }
                else {
                    for (let req of this.requirements) {
                        if (req.check())
                            return true;
                    }
                    return false;
                }
            }
            appendRequirement(item) {
                this.requirements.push(item);
            }
        }
        function getDomPath(el) {
            let stack = [el];
            while (el.parentNode != null) {
                let sibCount = 0;
                let sibIndex = 0;
                for (let i = 0; i < el.parentNode.childNodes.length; i++) {
                    let sib = el.parentNode.childNodes[i];
                    if (sib.nodeName == el.nodeName) {
                        if (sib === el) {
                            sibIndex = sibCount;
                        }
                        sibCount++;
                    }
                }
                stack.push(el);
                el = el.parentNode;
            }
            return stack.slice(1);
        }
        class Dialog {
            constructor() {
                this.Views = [];
                this.Fields = [];
                this.viewHistory = [];
                this.init();
                window.addEventListener('keydown', function (event) {
                    if (event.key === "Escape" && document.body.contains(this.element)) {
                        this.close();
                    }
                }.bind(this), false);
                document.body.addEventListener('mousedown', function (event) {
                    if (document.body.contains(this.element) && getDomPath(event.target).indexOf(this.element) === -1) {
                        this.close();
                    }
                }.bind(this));
            }
            init() {
                this.element = document.createElement('div');
                this.element.className = "dialog";
                this.headline = document.createElement('h1');
                this.element.appendChild(this.headline);
                this.mobileClose = document.createElement('div');
                this.mobileClose.className = 'mobile-close';
                this.element.appendChild(this.mobileClose);
                this.errorMsg = document.createElement('p');
                this.errorMsg.className = 'errorMsg';
                this.element.appendChild(this.errorMsg);
                this.viewContainer = document.createElement('div');
                this.viewContainer.className = 'view-container';
                this.element.appendChild(this.viewContainer);
                this.mobileClose.addEventListener('click', function () { this.close(); }.bind(this));
            }
            set(name, obj) {
                this.Fields[name] = obj;
            }
            setTitle(title) {
                this.headline.innerText = title;
            }
            setView(viewName, pushOnHistory = true) {
                if (typeof this.Views[viewName] !== 'undefined') {
                    while (this.viewContainer.firstChild)
                        this.viewContainer.removeChild(this.viewContainer.firstChild);
                    this.viewContainer.appendChild(this.Views[viewName].get());
                    if (pushOnHistory)
                        this.viewHistory.push(this.viewCurrent);
                    this.viewCurrent = viewName;
                    this.Views[viewName].onShow();
                }
            }
            remove(name) {
                delete this.Fields[name];
            }
            get(name) {
                return this.Fields[name];
            }
            showError(error) {
                this.errorMsg.innerText = error;
            }
            value() {
                let values = {};
                for (let key in this.Fields) {
                    if (this.Fields[key] instanceof Field) {
                        let val = this.Fields[key].value();
                        if (typeof val !== "undefined") {
                            values[key] = this.Fields[key].value();
                        }
                    }
                }
                return values;
            }
            requirementsCheck() {
                for (let key in this.Fields) {
                    if (this.Fields[key] instanceof Field) {
                        this.Fields[key].checkRequirements();
                    }
                }
                for (let viewKey in this.Views) {
                    for (let key in this.Views[viewKey].buttons) {
                        this.Views[viewKey].buttons[key].checkRequirements();
                    }
                }
            }
            load(path) {
                fetch(path).then((response) => response.text()).then((data) => {
                    let parser = new DOMParser();
                    let xmlDoc = parser.parseFromString(data, 'text/xml');
                    this.parseXML(xmlDoc);
                    this.initValues = this.value();
                    this.requirementsCheck();
                    this.show();
                });
            }
            parseXML(xmlDoc) {
                let xmlDialog = xmlDoc.getElementsByTagName('dialog')[0];
                this.setTitle(xmlDialog.getAttribute('title'));
                if (xmlDialog.hasAttribute('view')) {
                    this.initView = xmlDialog.getAttribute('view');
                }
                let xmlViews = xmlDialog.children;
                for (let xmlView of xmlViews) {
                    let view = new View(this);
                    if (xmlView.hasAttribute('name')) {
                        view.setName(xmlView.getAttribute('name'));
                    }
                    let xmlContents = xmlView.children;
                    for (let xmlContent of xmlContents) {
                        switch (xmlContent.tagName) {
                            case "html":
                                let contentHTML = new ContentHTML(xmlContent.innerHTML.trim());
                                view.appendContent(contentHTML);
                                break;
                            case "form":
                                let contentForm = new ContentForm();
                                if (xmlContent.hasAttribute('orientation') && xmlContent.getAttribute('orientation') === 'horizontal') {
                                    contentForm.setOrientation('horizontal');
                                }
                                let fields = xmlContent.children;
                                for (let fieldXML of fields) {
                                    let field;
                                    switch (fieldXML.tagName) {
                                        case "text":
                                            field = new FieldText(view);
                                            break;
                                        case "number":
                                            field = new FieldNumber(view);
                                            break;
                                        case "file":
                                            field = new FieldFile(view);
                                            break;
                                        case "date":
                                            field = new FieldDate(view);
                                            break;
                                        case "time":
                                            field = new FieldTime(view);
                                            break;
                                        case "datetime":
                                            field = new FieldDatetime(view);
                                            break;
                                        case "switch":
                                            field = new FieldSwitch(view);
                                            break;
                                        case "select":
                                            field = new FieldSelect(view);
                                            break;
                                        case "currency":
                                            field = new FieldCurrency(view);
                                            break;
                                        case "location":
                                            field = new FieldLocation(view);
                                            break;
                                        case "address":
                                            field = new FieldAddress(view);
                                            break;
                                        case "fileSelect":
                                            field = new FieldFileSelector(view);
                                            break;
                                        default:
                                            console.error('field type `' + fieldXML.tagName + '` unknown');
                                            continue;
                                    }
                                    if (!fieldXML.hasAttribute('name') && !fieldXML.hasAttribute('label')) {
                                        console.error('dialog fields must have a name and a label');
                                        continue;
                                    }
                                    field.setName(fieldXML.getAttribute('name'));
                                    field.setLabel(fieldXML.getAttribute('label'));
                                    if (fieldXML.hasAttribute('required')) {
                                        field.setRequired(fieldXML.getAttribute('required') === 'true');
                                    }
                                    if (fieldXML.hasAttribute('hint')) {
                                        field.setHint(fieldXML.getAttribute('hint'));
                                    }
                                    if (fieldXML.hasAttribute('visibility')) {
                                        field.setVisibility(fieldXML.getAttribute('visibility') === 'true');
                                    }
                                    let reqStack = [new RequirementAND([])];
                                    this.parseXMLRequirements(fieldXML, reqStack);
                                    if (reqStack[0].requirements.length > 0) {
                                        field.setRequirements(reqStack[0]);
                                    }
                                    if (field instanceof FieldText
                                        || field instanceof FieldNumber
                                        || field instanceof FieldFile
                                        || field instanceof FieldSwitch
                                        || field instanceof FieldCurrency) {
                                        if (fieldXML.hasAttribute('prefix')) {
                                            field.setPrefix(fieldXML.getAttribute('prefix'));
                                        }
                                        if (fieldXML.hasAttribute('suffix')) {
                                            field.setSuffix(fieldXML.getAttribute('suffix'));
                                        }
                                    }
                                    if (field instanceof FieldText
                                        || field instanceof FieldNumber
                                        || field instanceof FieldFile
                                        || field instanceof FieldCurrency) {
                                        if (fieldXML.hasAttribute('placeholder')) {
                                            field.setPlaceholder(fieldXML.getAttribute('placeholder'));
                                        }
                                    }
                                    if (field instanceof FieldText) {
                                        if (fieldXML.hasAttribute('maxLength')) {
                                            field.setMaxLength(Number(fieldXML.getAttribute('maxLength')));
                                        }
                                        if (fieldXML.hasAttribute('check')) {
                                            field.setCheck(fieldXML.getAttribute('check'), fieldXML.getAttribute('checkHint'));
                                        }
                                    }
                                    if (field instanceof FieldNumber
                                        || field instanceof FieldCurrency) {
                                        if (fieldXML.hasAttribute('min')) {
                                            field.setMin(Number(fieldXML.getAttribute('min')));
                                        }
                                        if (fieldXML.hasAttribute('max')) {
                                            field.setMax(Number(fieldXML.getAttribute('max')));
                                        }
                                        if (fieldXML.hasAttribute('step')) {
                                            field.setStep(Number(fieldXML.getAttribute('step')));
                                        }
                                    }
                                    if (field instanceof FieldLocation) {
                                        if (fieldXML.hasAttribute('map')) {
                                            field.setMap(fieldXML.getAttribute('map') === 'true');
                                        }
                                    }
                                    if (field instanceof FieldCurrency) {
                                        let currencies = [];
                                        for (let itemXML of fieldXML.children) {
                                            if (itemXML.tagName === 'item') {
                                                currencies.push(itemXML.textContent);
                                            }
                                        }
                                        field.setCurrencies(currencies);
                                    }
                                    if (field instanceof FieldSelect) {
                                        if (fieldXML.hasAttribute('type') && fieldXML.getAttribute('type') === 'search') {
                                            field.setType('search');
                                        }
                                        if (fieldXML.hasAttribute('source')) {
                                            field.setSource(fieldXML.getAttribute('source'));
                                        }
                                        for (let optionXML of fieldXML.children) {
                                            if (optionXML.tagName === 'option') {
                                                let item = {
                                                    headline: '',
                                                    value: ''
                                                };
                                                if (!optionXML.hasAttribute('value')) {
                                                    console.error('select option must have a value');
                                                    continue;
                                                }
                                                item.value = optionXML.getAttribute('value');
                                                item.headline = optionXML.textContent;
                                                field.appendItem(item);
                                            }
                                        }
                                    }
                                    if (field instanceof FieldAddress) {
                                        if (fieldXML.hasAttribute('streetNumber')) {
                                            field.setStreetNumber(Number(fieldXML.getAttribute('streetNumber')));
                                        }
                                        if (fieldXML.hasAttribute('houseName')) {
                                            field.setHouseName(fieldXML.getAttribute('houseName'));
                                        }
                                        if (fieldXML.hasAttribute('streetNumberSuffix')) {
                                            field.setStreetNumberSuffix(fieldXML.getAttribute('streetNumberSuffix'));
                                        }
                                        if (fieldXML.hasAttribute('streetName')) {
                                            field.setStreetName(fieldXML.getAttribute('streetName'));
                                        }
                                        if (fieldXML.hasAttribute('streetType')) {
                                            field.setStreetType(fieldXML.getAttribute('streetType'));
                                        }
                                        if (fieldXML.hasAttribute('streetDirection')) {
                                            field.setStreetDirection(fieldXML.getAttribute('streetDirection'));
                                        }
                                        if (fieldXML.hasAttribute('addressType')) {
                                            field.setAddressType(fieldXML.getAttribute('addressType'));
                                        }
                                        if (fieldXML.hasAttribute('addressTypeIdentifier')) {
                                            field.setAddressTypeIdentifier(fieldXML.getAttribute('addressTypeIdentifier'));
                                        }
                                        if (fieldXML.hasAttribute('localMuniciplity')) {
                                            field.setLocalMuniciplity(fieldXML.getAttribute('localMuniciplity'));
                                        }
                                        if (fieldXML.hasAttribute('city')) {
                                            field.setCity(fieldXML.getAttribute('city'));
                                        }
                                        if (fieldXML.hasAttribute('governingDistrict')) {
                                            field.setGoverningDistrict(fieldXML.getAttribute('governingDistrict'));
                                        }
                                        if (fieldXML.hasAttribute('postalArea')) {
                                            field.setPostalArea(fieldXML.getAttribute('postalArea'));
                                        }
                                        if (fieldXML.hasAttribute('country')) {
                                            field.setCountry(fieldXML.getAttribute('country'));
                                        }
                                    }
                                    if (field instanceof FieldFileSelector) {
                                        if (fieldXML.hasAttribute('source')) {
                                            field.setSource(fieldXML.getAttribute('source'));
                                        }
                                    }
                                    if (field instanceof FieldText
                                        || field instanceof FieldDate
                                        || field instanceof FieldTime
                                        || field instanceof FieldDatetime
                                        || field instanceof FieldFileSelector) {
                                        if (fieldXML.hasAttribute('value')) {
                                            field.setValue(fieldXML.getAttribute('value'));
                                        }
                                    }
                                    else if (field instanceof FieldNumber) {
                                        if (fieldXML.hasAttribute('value')) {
                                            field.setValue(Number(fieldXML.getAttribute('value')));
                                        }
                                    }
                                    else if (field instanceof FieldSwitch) {
                                        if (fieldXML.hasAttribute('value')) {
                                            field.setValue(fieldXML.getAttribute('value') === 'true');
                                        }
                                    }
                                    else if (field instanceof FieldSelect) {
                                        if (fieldXML.hasAttribute('value')) {
                                            field.setValue(fieldXML.getAttribute('value'));
                                        }
                                    }
                                    else if (field instanceof FieldCurrency) {
                                        if (fieldXML.hasAttribute('value') && fieldXML.hasAttribute('currency')) {
                                            field.setValue({
                                                Value: Number(fieldXML.getAttribute('value')),
                                                Currency: fieldXML.getAttribute('currency'),
                                            });
                                        }
                                    }
                                    else if (field instanceof FieldLocation) {
                                        if (fieldXML.hasAttribute('latitude') && fieldXML.hasAttribute('longitude')) {
                                            field.setValue({
                                                Latitude: Number(fieldXML.getAttribute('latitude')),
                                                Longitude: Number(fieldXML.getAttribute('longitude')),
                                            });
                                        }
                                    }
                                    contentForm.appendField(field);
                                }
                                view.appendContent(contentForm);
                                break;
                            case 'button':
                                let button = new Button(this);
                                if (xmlContent.hasAttribute('label')) {
                                    button.setLabel(xmlContent.getAttribute('label'));
                                }
                                if (xmlContent.hasAttribute('action')) {
                                    button.setAction(xmlContent.getAttribute('action'));
                                }
                                if (xmlContent.hasAttribute('visibility')) {
                                    button.setVisibility(xmlContent.getAttribute('visibility') === 'true');
                                }
                                let reqStack = [new RequirementAND([])];
                                this.parseXMLRequirements(xmlContent, reqStack);
                                if (reqStack[0].requirements.length > 0) {
                                    button.setRequirements(reqStack[0]);
                                }
                                view.appendButton(button);
                                break;
                            default:
                        }
                    }
                    this.Views[xmlView.getAttribute('name')] = view;
                }
            }
            parseXMLRequirements(item, stack) {
                for (let itemXML of item.children) {
                    let lastItem = stack[stack.length - 1];
                    switch (itemXML.tagName) {
                        case 'requirement':
                            let requirement = new Requirement(this);
                            if (itemXML.hasAttribute('name')) {
                                requirement.name = itemXML.getAttribute('name');
                            }
                            if (itemXML.hasAttribute('pattern')) {
                                requirement.pattern = itemXML.getAttribute('pattern');
                            }
                            if (itemXML.hasAttribute('value')) {
                                requirement.value = itemXML.getAttribute('value');
                            }
                            if (itemXML.hasAttribute('greater')) {
                                requirement.greater = itemXML.getAttribute('greater');
                            }
                            if (itemXML.hasAttribute('less')) {
                                requirement.less = itemXML.getAttribute('less');
                            }
                            lastItem.appendRequirement(requirement);
                            break;
                        case 'and':
                            let requirementAND = new RequirementAND([]);
                            if (lastItem instanceof RequirementOR) {
                                lastItem.appendRequirement(requirementAND);
                            }
                            stack.push(requirementAND);
                            this.parseXMLRequirements(itemXML, stack);
                            stack.pop();
                            break;
                        case 'or':
                            let requirementOR = new RequirementOR([]);
                            if (lastItem instanceof RequirementAND) {
                                lastItem.appendRequirement(requirementOR);
                            }
                            stack.push(requirementOR);
                            this.parseXMLRequirements(itemXML, stack);
                            stack.pop();
                            break;
                    }
                }
            }
            show() {
                document.body.appendChild(this.element);
                if (typeof this.initView === "string") {
                    this.setView(this.initView);
                }
                else {
                    this.setView(Object.keys(this.Views)[0]);
                }
            }
            close(confirmation = true) {
                if (document.body.contains(this.element)) {
                    if (confirmation && JSON.stringify(this.initValues) !== JSON.stringify(this.value())) {
                        if (confirm('are you sure, you want to leave unsaved?')) {
                            document.body.removeChild(this.element);
                        }
                    }
                    else {
                        document.body.removeChild(this.element);
                    }
                }
            }
        }
        Dialog_1.Dialog = Dialog;
        class View {
            constructor(context) {
                this.Content = [];
                this.buttons = [];
                this.Context = context;
                this.view = document.createElement('div');
                this.view.className = 'view';
                this.content = document.createElement('div');
                this.content.className = 'content-container';
                this.view.appendChild(this.content);
                this.buttonContainer = document.createElement('div');
                this.buttonContainer.className = 'button-container';
                this.view.appendChild(this.buttonContainer);
            }
            get() {
                return this.view;
            }
            onShow() {
                for (let cont of this.Content) {
                    if (cont instanceof ContentForm) {
                        cont.onShow();
                    }
                }
            }
            setName(name) {
                this.name = name;
            }
            appendContent(conItem) {
                this.Content.push(conItem);
                this.content.appendChild(conItem.get());
            }
            appendButton(button) {
                this.buttons.push(button);
                this.buttonContainer.appendChild(button.get());
            }
        }
        Dialog_1.View = View;
        class ContentHTML {
            constructor(definition) {
                this.HTML = definition;
                this.content = document.createElement('div');
                this.content.className = 'dialog-content html';
                this.content.innerHTML = this.HTML;
            }
            get() {
                return this.content;
            }
        }
        class ContentForm {
            constructor() {
                this.Fields = [];
                this.Orientation = 'vertical';
                this.form = document.createElement('div');
                this.form.className = 'dialog-content form';
            }
            get() {
                return this.form;
            }
            onShow() {
                for (let field of this.Fields) {
                    field.onShow();
                }
            }
            setOrientation(orientation) {
                this.Orientation = orientation;
                this.form.setAttribute('orientation', this.Orientation);
            }
            appendField(field) {
                this.Fields.push(field);
                this.form.appendChild(field.get());
            }
        }
        class Field {
            constructor(context) {
                this.HTMLElements = [];
                this.required = false;
                this.context = context;
            }
            get() {
                return document.createElement('div');
            }
            value() {
                return undefined;
            }
            onChange() {
                this.context.Context.requirementsCheck();
            }
            onShow() {
            }
            setName(name) {
                this.context.Context.remove(this.Name);
                this.context.Context.set(name, this);
                this.Name = name;
            }
            setLabel(label) {
                this.label = label;
                if (this.required) {
                    this.HTMLElements['label'].innerText = label + '*';
                }
                else {
                    this.HTMLElements['label'].innerText = label;
                }
            }
            setVisibility(visibility) {
                this.visibility = visibility;
                if (visibility) {
                    this.container.style.display = '';
                }
                else {
                    this.container.style.display = 'none';
                }
            }
            setRequired(required) {
                this.required = required;
                this.setLabel(this.label);
            }
            setHint(hint) {
                if (this.HTMLElements['supportText'] instanceof HTMLDivElement) {
                    this.Hint = hint;
                    this.HTMLElements['supportText'].innerText = this.Hint;
                }
            }
            setPrefix(prefix) {
                this.HTMLElements['prefix'].innerText = prefix;
                if (prefix.trim() === '') {
                    this.HTMLElements['prefix'].style.display = 'none';
                }
                else {
                    this.HTMLElements['prefix'].style.display = '';
                }
            }
            setSuffix(suffix) {
                this.HTMLElements['suffix'].innerText = suffix;
                if (suffix.trim() === '') {
                    this.HTMLElements['suffix'].style.display = 'none';
                }
                else {
                    this.HTMLElements['suffix'].style.display = '';
                }
            }
            setRequirements(requirements) {
                this.Requirements = requirements;
            }
            checkRequirements() {
                if (typeof this.Requirements !== "undefined") {
                    if (this.Requirements.check()) {
                        this.setVisibility(true);
                    }
                    else {
                        this.setVisibility(false);
                    }
                }
            }
        }
        class FieldText extends Field {
            constructor(context) {
                super(context);
                this.init();
            }
            init() {
                this.container = document.createElement('div');
                this.container.className = 'field-container';
                let inputContainer = document.createElement('div');
                inputContainer.className = 'input';
                this.container.appendChild(inputContainer);
                this.HTMLElements['prefix'] = document.createElement('div');
                this.HTMLElements['prefix'].className = 'field-prefix';
                inputContainer.appendChild(this.HTMLElements['prefix']);
                this.HTMLElements['input'] = document.createElement('input');
                this.HTMLElements['input'].type = 'text';
                inputContainer.appendChild(this.HTMLElements['input']);
                this.HTMLElements['suffix'] = document.createElement('div');
                this.HTMLElements['suffix'].className = 'field-suffix';
                inputContainer.appendChild(this.HTMLElements['suffix']);
                this.HTMLElements['label'] = document.createElement('div');
                this.HTMLElements['label'].className = 'field-label';
                this.HTMLElements['label'].innerText = this.Label;
                this.container.appendChild(this.HTMLElements['label']);
                this.HTMLElements['supportText'] = document.createElement('div');
                this.HTMLElements['supportText'].className = 'field-supportText';
                this.container.appendChild(this.HTMLElements['supportText']);
                if (typeof this.Hint === "string") {
                    this.HTMLElements['supportText'].innerText = this.Hint;
                }
                this.HTMLElements['count'] = document.createElement('div');
                this.HTMLElements['count'].className = 'field-count';
                this.container.appendChild(this.HTMLElements['count']);
                this.container.addEventListener('click', function (event) {
                    if (event.target !== this.HTMLElements['input']) {
                        this.HTMLElements['input'].focus();
                    }
                }.bind(this), false);
                this.HTMLElements['input'].addEventListener('focus', function () {
                    this.container.className = 'field-container focus';
                }.bind(this), false);
                this.HTMLElements['input'].addEventListener('focusout', this.inputCheck.bind(this), false);
                this.HTMLElements['input'].addEventListener('keyup', this.onChange.bind(this), false);
                this.HTMLElements['input'].addEventListener('keydown', this.onChange.bind(this), false);
            }
            get() {
                return this.container;
            }
            check() {
                if (this.required && this.HTMLElements['input'].value === '') {
                    return ERROR_MSG_REQUIRED;
                }
                if (typeof this.pattern === "string" && this.HTMLElements['input'].value.length > 0) {
                    const regex = new RegExp(this.pattern);
                    if (regex.test(this.HTMLElements['input'].value)) {
                        return undefined;
                    }
                    else {
                        if (typeof this.CheckHint === 'string') {
                            return this.CheckHint;
                        }
                        else {
                            return 'Values does not match `' + this.pattern + '`';
                        }
                    }
                }
                else
                    return undefined;
            }
            value() {
                if (this.HTMLElements['input'].value.length === 0)
                    return undefined;
                else
                    return this.HTMLElements['input'].value;
            }
            setValue(value) {
                this.HTMLElements['input'].value = value;
                this.inputCheck();
                this.onChange();
            }
            setPlaceholder(placeholder) {
                this.HTMLElements['input'].placeholder = placeholder;
            }
            setMaxLength(maxLength) {
                this.MaxLength = maxLength;
                this.HTMLElements['input'].maxLength = maxLength;
                this.onChange();
            }
            setCheck(check, hint) {
                this.pattern = check;
                this.CheckHint = hint;
                this.inputCheck();
            }
            inputCheck() {
                let classes = ['field-container'];
                if (this.HTMLElements['input'].value !== "") {
                    classes.push('filled');
                }
                let check = this.check();
                if (typeof check !== 'undefined') {
                    classes.push('error');
                    this.HTMLElements['supportText'].innerText = check;
                }
                else if (typeof this.Hint === "string") {
                    this.HTMLElements['supportText'].innerText = this.Hint;
                }
                this.container.className = classes.join(' ');
            }
            onChange() {
                this.context.Context.requirementsCheck();
                if (typeof this.MaxLength === "number") {
                    this.HTMLElements['count'].innerText = this.HTMLElements['input'].value.length + " / " + this.MaxLength;
                }
            }
        }
        Dialog_1.FieldText = FieldText;
        class FieldNumber extends Field {
            constructor(context) {
                super(context);
                this.init();
            }
            init() {
                this.container = document.createElement('div');
                this.container.className = 'field-container';
                let inputContainer = document.createElement('div');
                inputContainer.className = 'input';
                this.container.appendChild(inputContainer);
                this.HTMLElements['prefix'] = document.createElement('div');
                this.HTMLElements['prefix'].className = 'field-prefix';
                inputContainer.appendChild(this.HTMLElements['prefix']);
                this.HTMLElements['input'] = document.createElement('input');
                this.HTMLElements['input'].type = 'number';
                this.HTMLElements['input'].style.textAlign = 'right';
                inputContainer.appendChild(this.HTMLElements['input']);
                this.HTMLElements['suffix'] = document.createElement('div');
                this.HTMLElements['suffix'].className = 'field-suffix';
                inputContainer.appendChild(this.HTMLElements['suffix']);
                this.HTMLElements['label'] = document.createElement('div');
                this.HTMLElements['label'].className = 'field-label';
                this.HTMLElements['label'].innerText = this.Label;
                this.container.appendChild(this.HTMLElements['label']);
                this.HTMLElements['supportText'] = document.createElement('div');
                this.HTMLElements['supportText'].className = 'field-supportText';
                this.container.appendChild(this.HTMLElements['supportText']);
                this.container.addEventListener('click', function (event) {
                    if (event.target !== this.HTMLElements['input'])
                        this.HTMLElements['input'].focus();
                }.bind(this), false);
                this.HTMLElements['input'].addEventListener('focus', function () {
                    this.container.className = 'field-container focus';
                }.bind(this), false);
                this.HTMLElements['input'].addEventListener('focusout', this.inputCheck.bind(this), false);
                this.HTMLElements['input'].addEventListener('change', this.onChange.bind(this));
            }
            inputCheck() {
                let classes = ['field-container'];
                if (this.HTMLElements['input'].value !== "") {
                    classes.push('filled');
                }
                let check = this.check();
                if (typeof check !== "undefined") {
                    classes.push('error');
                    this.HTMLElements['supportText'].innerText = check;
                }
                else if (typeof this.Hint === "string") {
                    this.HTMLElements['supportText'].innerText = this.Hint;
                }
                this.container.className = classes.join(' ');
            }
            get() {
                return this.container;
            }
            check() {
                if (this.required && this.HTMLElements['input'].value.length === 0) {
                    return ERROR_MSG_REQUIRED;
                }
                if (typeof this.Min === "number" && this.Min > Number(this.HTMLElements['input'].value)) {
                    return "Value must be greater or equal than " + this.Min;
                }
                if (typeof this.Max === "number" && this.Max < Number(this.HTMLElements['input'].value)) {
                    return "Value must be lower or equal than " + this.Max;
                }
                if (typeof this.Step === "number" && parseFloat((Number(this.HTMLElements['input'].value) / this.Step).toPrecision(12)) % 1 !== 0) {
                    return "Value must be in step of " + this.Step;
                }
                return undefined;
            }
            value() {
                if (this.HTMLElements['input'].value.length === 0)
                    return undefined;
                else
                    return Number(this.HTMLElements['input'].value);
            }
            setValue(value) {
                this.HTMLElements['input'].value = value;
                this.inputCheck();
            }
            setPlaceholder(placeholder) {
                this.HTMLElements['input'].placeholder = placeholder;
            }
            setMin(min) {
                this.Min = min;
                this.HTMLElements['input'].min = String(min);
            }
            setMax(max) {
                this.Max = max;
                this.HTMLElements['input'].max = String(max);
            }
            setStep(step) {
                this.Step = step;
                this.HTMLElements['input'].step = String(step);
            }
        }
        Dialog_1.FieldNumber = FieldNumber;
        class FieldFile extends Field {
            constructor(context) {
                super(context);
                this.init();
            }
            init() {
                this.container = document.createElement('div');
                this.container.className = 'field-container';
                let inputContainer = document.createElement('div');
                inputContainer.className = 'input';
                this.container.appendChild(inputContainer);
                this.HTMLElements['prefix'] = document.createElement('div');
                this.HTMLElements['prefix'].className = 'field-prefix';
                inputContainer.appendChild(this.HTMLElements['prefix']);
                this.HTMLElements['input'] = document.createElement('input');
                this.HTMLElements['input'].type = 'file';
                this.HTMLElements['input'].style.textAlign = 'right';
                inputContainer.appendChild(this.HTMLElements['input']);
                this.HTMLElements['suffix'] = document.createElement('div');
                this.HTMLElements['suffix'].className = 'field-suffix';
                inputContainer.appendChild(this.HTMLElements['suffix']);
                this.HTMLElements['label'] = document.createElement('div');
                this.HTMLElements['label'].className = 'field-label';
                this.HTMLElements['label'].innerText = this.Label;
                this.container.appendChild(this.HTMLElements['label']);
                this.HTMLElements['supportText'] = document.createElement('div');
                this.HTMLElements['supportText'].className = 'field-supportText';
                this.container.appendChild(this.HTMLElements['supportText']);
                this.container.addEventListener('click', function (event) {
                    if (event.target !== this.HTMLElements['input'])
                        this.HTMLElements['input'].focus();
                }.bind(this), false);
                this.HTMLElements['input'].addEventListener('focus', function () {
                    this.container.className = 'field-container focus';
                }.bind(this), false);
                this.HTMLElements['input'].addEventListener('focusout', this.inputCheck.bind(this), false);
                this.HTMLElements['input'].addEventListener('change', this.onChange.bind(this));
            }
            inputCheck() {
                let classes = ['field-container'];
                if (this.HTMLElements['input'].value !== "") {
                    classes.push('filled');
                }
                let check = this.check();
                if (typeof check !== "undefined") {
                    classes.push('error');
                    this.HTMLElements['supportText'].innerText = check;
                }
                else if (typeof this.Hint === "string") {
                    this.HTMLElements['supportText'].innerText = this.Hint;
                }
                this.container.className = classes.join(' ');
            }
            get() {
                return this.container;
            }
            check() {
                if (this.required && this.HTMLElements['input'].value.length === 0) {
                    return ERROR_MSG_REQUIRED;
                }
                return undefined;
            }
            value() {
                if (this.HTMLElements['input'].value.length === 0)
                    return undefined;
                else
                    return Number(this.HTMLElements['input'].value);
            }
            setValue(value) {
                this.HTMLElements['input'].value = value;
                this.inputCheck();
            }
            setPlaceholder(placeholder) {
                this.HTMLElements['input'].placeholder = placeholder;
            }
        }
        Dialog_1.FieldFile = FieldFile;
        class FieldDate extends Field {
            constructor(context) {
                super(context);
                this.init();
            }
            init() {
                this.container = document.createElement('div');
                this.container.className = 'field-container';
                let inputContainer = document.createElement('div');
                inputContainer.className = 'input';
                this.container.appendChild(inputContainer);
                this.HTMLElements['input'] = document.createElement('input');
                this.HTMLElements['input'].type = 'date';
                this.HTMLElements['input'].pattern = "\d{4}-\d{2}-\d{2}";
                inputContainer.appendChild(this.HTMLElements['input']);
                this.HTMLElements['label'] = document.createElement('div');
                this.HTMLElements['label'].className = 'field-label';
                this.HTMLElements['label'].innerText = this.Label;
                this.container.appendChild(this.HTMLElements['label']);
                this.HTMLElements['supportText'] = document.createElement('div');
                this.HTMLElements['supportText'].className = 'field-supportText';
                this.container.appendChild(this.HTMLElements['supportText']);
                this.container.addEventListener('click', function (event) {
                    if (event.target !== this.HTMLElements['input'])
                        this.HTMLElements['input'].focus();
                }.bind(this), false);
                this.HTMLElements['input'].addEventListener('focus', function () {
                    this.container.className = 'field-container focus';
                }.bind(this), false);
                this.HTMLElements['input'].addEventListener('focusout', this.inputCheck.bind(this), false);
                this.HTMLElements['input'].addEventListener('change', this.onChange.bind(this));
            }
            inputCheck() {
                let classes = ['field-container'];
                if (this.HTMLElements['input'].value !== "") {
                    classes.push('filled');
                }
                let check = this.check();
                if (typeof check !== "undefined") {
                    classes.push('error');
                    this.HTMLElements['supportText'].innerText = check;
                }
                else if (typeof this.Hint === "string") {
                    this.HTMLElements['supportText'].innerText = this.Hint;
                }
                this.container.className = classes.join(' ');
            }
            get() {
                return this.container;
            }
            check() {
                if (this.required && this.HTMLElements['input'].value.length === 0) {
                    return ERROR_MSG_REQUIRED;
                }
                return undefined;
            }
            value() {
                if (this.HTMLElements['input'].value.length === 0)
                    return undefined;
                else
                    return this.HTMLElements['input'].value;
            }
            setValue(date) {
                this.HTMLElements['input'].value = date;
                this.inputCheck();
            }
        }
        Dialog_1.FieldDate = FieldDate;
        class FieldTime extends Field {
            constructor(context) {
                super(context);
                this.init();
            }
            init() {
                this.container = document.createElement('div');
                this.container.className = 'field-container';
                let inputContainer = document.createElement('div');
                inputContainer.className = 'input';
                this.container.appendChild(inputContainer);
                this.HTMLElements['input'] = document.createElement('input');
                this.HTMLElements['input'].type = 'time';
                this.HTMLElements['input'].pattern = "\d{2}:\d{2}";
                inputContainer.appendChild(this.HTMLElements['input']);
                this.HTMLElements['label'] = document.createElement('div');
                this.HTMLElements['label'].className = 'field-label';
                this.HTMLElements['label'].innerText = this.Label;
                this.container.appendChild(this.HTMLElements['label']);
                this.HTMLElements['supportText'] = document.createElement('div');
                this.HTMLElements['supportText'].className = 'field-supportText';
                this.container.appendChild(this.HTMLElements['supportText']);
                this.container.addEventListener('click', function (event) {
                    if (event.target !== this.HTMLElements['input'])
                        this.HTMLElements['input'].focus();
                }.bind(this), false);
                this.HTMLElements['input'].addEventListener('focus', function () {
                    this.container.className = 'field-container focus';
                }.bind(this), false);
                this.HTMLElements['input'].addEventListener('focusout', this.inputCheck.bind(this), false);
                this.HTMLElements['input'].addEventListener('change', this.onChange.bind(this));
            }
            inputCheck() {
                let classes = ['field-container'];
                if (this.HTMLElements['input'].value !== "") {
                    classes.push('filled');
                }
                let check = this.check();
                if (typeof check !== "undefined") {
                    classes.push('error');
                    this.HTMLElements['supportText'].innerText = check;
                }
                else if (typeof this.Hint === "string") {
                    this.HTMLElements['supportText'].innerText = this.Hint;
                }
                this.container.className = classes.join(' ');
            }
            get() {
                return this.container;
            }
            check() {
                if (this.required && this.HTMLElements['input'].value.length === 0) {
                    return ERROR_MSG_REQUIRED;
                }
                return undefined;
            }
            value() {
                if (this.HTMLElements['input'].value.length === 0)
                    return undefined;
                else
                    return this.HTMLElements['input'].value;
            }
            setValue(date) {
                this.HTMLElements['input'].value = date;
                this.inputCheck();
            }
        }
        Dialog_1.FieldTime = FieldTime;
        class FieldDatetime extends Field {
            constructor(context) {
                super(context);
                this.init();
            }
            init() {
                this.container = document.createElement('div');
                this.container.className = 'field-container';
                let inputContainer = document.createElement('div');
                inputContainer.className = 'input';
                this.container.appendChild(inputContainer);
                this.HTMLElements['input'] = document.createElement('input');
                this.HTMLElements['input'].type = 'datetime-local';
                this.HTMLElements['input'].pattern = "[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}";
                inputContainer.appendChild(this.HTMLElements['input']);
                this.HTMLElements['label'] = document.createElement('div');
                this.HTMLElements['label'].className = 'field-label';
                this.HTMLElements['label'].innerText = this.Label;
                this.container.appendChild(this.HTMLElements['label']);
                this.HTMLElements['supportText'] = document.createElement('div');
                this.HTMLElements['supportText'].className = 'field-supportText';
                this.container.appendChild(this.HTMLElements['supportText']);
                this.container.addEventListener('click', function (event) {
                    if (event.target !== this.HTMLElements['input'])
                        this.HTMLElements['input'].focus();
                }.bind(this), false);
                this.HTMLElements['input'].addEventListener('focus', function () {
                    this.container.className = 'field-container focus';
                }.bind(this), false);
                this.HTMLElements['input'].addEventListener('focusout', this.inputCheck.bind(this), false);
                this.HTMLElements['input'].addEventListener('change', this.onChange.bind(this));
            }
            inputCheck() {
                let classes = ['field-container'];
                if (this.HTMLElements['input'].value !== "") {
                    classes.push('filled');
                }
                let check = this.check();
                if (typeof check !== "undefined") {
                    classes.push('error');
                    this.HTMLElements['supportText'].innerText = check;
                }
                else if (typeof this.Hint === "string") {
                    this.HTMLElements['supportText'].innerText = this.Hint;
                }
                this.container.className = classes.join(' ');
            }
            get() {
                return this.container;
            }
            check() {
                if (this.required && this.HTMLElements['input'].value.length === 0) {
                    return ERROR_MSG_REQUIRED;
                }
                return undefined;
            }
            value() {
                if (this.HTMLElements['input'].value.length === 0)
                    return undefined;
                else
                    return this.HTMLElements['input'].value;
            }
            setValue(date) {
                this.HTMLElements['input'].value = date;
                this.inputCheck();
            }
        }
        Dialog_1.FieldDatetime = FieldDatetime;
        class FieldSwitch extends Field {
            constructor(context) {
                super(context);
                this.init();
            }
            init() {
                this.container = document.createElement('div');
                this.container.className = 'field-container switch';
                this.HTMLElements['label'] = document.createElement("div");
                this.HTMLElements['label'].className = "field-label";
                this.container.appendChild(this.HTMLElements['label']);
                this.HTMLElements['supportText'] = document.createElement('div');
                this.HTMLElements['supportText'].className = 'field-supportText';
                this.container.appendChild(this.HTMLElements['supportText']);
                let switchContainer = document.createElement("div");
                switchContainer.className = "field-switch-container";
                this.container.appendChild(switchContainer);
                this.HTMLElements['prefix'] = document.createElement('div');
                this.HTMLElements['prefix'].className = 'field-prefix';
                switchContainer.appendChild(this.HTMLElements['prefix']);
                let switchObj = document.createElement("label");
                switchContainer.appendChild(switchObj);
                this.HTMLElements['input'] = document.createElement("input");
                this.HTMLElements['input'].type = "checkbox";
                switchObj.appendChild(this.HTMLElements['input']);
                let slider = document.createElement("span");
                slider.className = "field-switch-slider";
                switchObj.appendChild(slider);
                this.HTMLElements['suffix'] = document.createElement('div');
                this.HTMLElements['suffix'].className = 'field-suffix';
                switchContainer.appendChild(this.HTMLElements['suffix']);
                this.HTMLElements['input'].addEventListener('change', this.onChange.bind(this));
            }
            onChange() {
                super.onChange();
                let classes = ['field-container switch'];
                let check = this.check();
                if (typeof check !== "undefined") {
                    classes.push('error');
                    this.HTMLElements['supportText'].innerText = check;
                }
                else if (typeof this.Hint === "string") {
                    this.HTMLElements['supportText'].innerText = this.Hint;
                }
                else {
                    this.HTMLElements['supportText'].innerText = '';
                }
                this.container.className = classes.join(' ');
            }
            get() {
                return this.container;
            }
            check() {
                if (this.required && !this.HTMLElements['input'].checked) {
                    return ERROR_MSG_REQUIRED;
                }
                return undefined;
            }
            value() {
                return this.HTMLElements['input'].checked;
            }
            setValue(value) {
                this.HTMLElements['input'].checked = value;
            }
        }
        Dialog_1.FieldSwitch = FieldSwitch;
        class FieldSelect extends Field {
            constructor(context) {
                super(context);
                this.items = [];
                this.type = 'static';
                this.init();
            }
            init() {
                this.container = document.createElement('div');
                this.container.className = 'field-container select';
                let inputContainer = document.createElement('div');
                inputContainer.className = 'input';
                this.container.appendChild(inputContainer);
                this.HTMLElements['prefix'] = document.createElement('div');
                this.HTMLElements['prefix'].className = 'field-prefix';
                inputContainer.appendChild(this.HTMLElements['prefix']);
                this.HTMLElements['input'] = document.createElement('div');
                this.HTMLElements['input'].className = 'select';
                this.HTMLElements['input'].tabIndex = 0;
                inputContainer.appendChild(this.HTMLElements['input']);
                this.HTMLElements['value'] = document.createElement('div');
                this.HTMLElements['value'].className = 'value';
                this.HTMLElements['value'].innerText = 'select item';
                this.HTMLElements['input'].appendChild(this.HTMLElements['value']);
                this.HTMLElements['search'] = document.createElement('input');
                this.HTMLElements['search'].className = 'value';
                this.HTMLElements['search'].style.display = 'none';
                this.HTMLElements['input'].appendChild(this.HTMLElements['search']);
                this.HTMLElements['options'] = document.createElement('div');
                this.HTMLElements['options'].className = 'options';
                this.HTMLElements['input'].appendChild(this.HTMLElements['options']);
                this.HTMLElements['suffix'] = document.createElement('div');
                this.HTMLElements['suffix'].className = 'field-suffix';
                inputContainer.appendChild(this.HTMLElements['suffix']);
                this.HTMLElements['label'] = document.createElement('div');
                this.HTMLElements['label'].className = 'field-label';
                this.HTMLElements['label'].innerText = this.Label;
                this.container.appendChild(this.HTMLElements['label']);
                this.HTMLElements['supportText'] = document.createElement('div');
                this.HTMLElements['supportText'].className = 'field-supportText';
                this.container.appendChild(this.HTMLElements['supportText']);
                if (typeof this.Hint === "string") {
                    this.HTMLElements['supportText'].innerText = this.Hint;
                }
                this.HTMLElements['count'] = document.createElement('div');
                this.HTMLElements['count'].className = 'field-count';
                this.container.appendChild(this.HTMLElements['count']);
                this.container.addEventListener('click', function (event) {
                    if (event.target !== this.HTMLElements['input']
                        && event.target !== this.HTMLElements['search']) {
                        this.HTMLElements['input'].focus();
                    }
                }.bind(this), false);
                this.HTMLElements['input'].addEventListener('focus', function () {
                    this.container.className = 'field-container select focus';
                }.bind(this), false);
                this.HTMLElements['input'].addEventListener('focusout', this.inputCheck.bind(this), false);
                this.HTMLElements['search'].addEventListener('focus', function () {
                    this.container.className = 'field-container select focus';
                }.bind(this), false);
                this.HTMLElements['search'].addEventListener('focusout', this.inputCheck.bind(this), false);
                this.HTMLElements['search'].addEventListener('keyup', this.getSearch.bind(this));
            }
            get() {
                return this.container;
            }
            check() {
                if (this.required && typeof this.selectedItem === 'undefined') {
                    return ERROR_MSG_REQUIRED;
                }
                return undefined;
            }
            value() {
                if (typeof this.selectedItem === 'undefined')
                    return undefined;
                else
                    return this.selectedItem.value;
            }
            setValue(value) {
                if (typeof this.items[value] === 'undefined') {
                    if (this.type === 'search') {
                        this.HTMLElements['search'].value = value;
                        this.getSearch(function () {
                            this.setValue(value);
                        }.bind(this));
                    }
                    else {
                        console.error('select does not have the value ' + value);
                    }
                    return;
                }
                this.selectedItem = this.items[value];
                if (this.type === 'static') {
                    while (this.HTMLElements['value'].firstChild)
                        this.HTMLElements['value'].removeChild(this.HTMLElements['value'].firstChild);
                    this.HTMLElements['value'].appendChild(this.buildItem(this.items[value]));
                }
                else {
                    this.HTMLElements['search'].value = this.items[value].headline;
                }
                this.inputCheck();
                this.onChange();
            }
            setType(type) {
                this.type = type;
                if (this.type === 'search') {
                    this.HTMLElements['search'].style.display = '';
                    this.HTMLElements['value'].style.display = 'none';
                }
                else {
                    this.HTMLElements['search'].style.display = 'none';
                    this.HTMLElements['value'].style.display = '';
                }
            }
            setSource(source) {
                this.source = source;
            }
            appendItem(item) {
                this.items[item.value] = item;
                let option = this.buildItem(item);
                this.HTMLElements['options'].appendChild(option);
                option.addEventListener('click', function (event) {
                    event.preventDefault();
                    event.stopImmediatePropagation();
                    document.activeElement.blur();
                    this.setValue(item.value);
                }.bind(this), false);
            }
            clearItems() {
                while (this.HTMLElements['options'].firstChild)
                    this.HTMLElements['options'].removeChild(this.HTMLElements['options'].firstChild);
                this.items = [];
            }
            inputCheck() {
                let classes = ['field-container select'];
                if (typeof this.selectedItem !== "undefined") {
                    classes.push('filled');
                }
                let check = this.check();
                if (typeof check !== 'undefined') {
                    classes.push('error');
                    this.HTMLElements['supportText'].innerText = check;
                }
                else if (typeof this.Hint === "string") {
                    this.HTMLElements['supportText'].innerText = this.Hint;
                }
                this.container.className = classes.join(' ');
            }
            buildItem(definition) {
                let item = document.createElement('div');
                item.className = 'item';
                if (typeof definition.icon === 'string') {
                    let left = document.createElement('div');
                    left.className = 'flex-item';
                    item.appendChild(left);
                    let img = document.createElement('img');
                    img.src = definition.icon;
                    left.appendChild(img);
                    if (typeof definition.iconStyle === "string") {
                        img.className = definition.iconStyle;
                    }
                }
                let right = document.createElement('div');
                right.className = 'flex-item';
                item.appendChild(right);
                let headline = document.createElement('h4');
                headline.innerText = definition.headline;
                right.appendChild(headline);
                if (typeof definition.supportText === 'string') {
                    right.className = 'flex-item top';
                    let supportText = document.createElement('p');
                    supportText.innerText = definition.supportText;
                    right.appendChild(supportText);
                }
                return item;
            }
            getSearch(callback = undefined) {
                if (this.HTMLElements['search'].value.length >= 3) {
                    fetch(this.source + '?q=' + this.HTMLElements['search'].value)
                        .then((response) => response.json())
                        .then((data) => {
                        this.clearItems();
                        if (typeof data === 'object' && data !== null) {
                            for (let item of data) {
                                this.appendItem(item);
                            }
                        }
                        if (typeof callback === 'function')
                            callback();
                    });
                }
                else {
                    this.clearItems();
                }
            }
        }
        Dialog_1.FieldSelect = FieldSelect;
        class FieldCurrency extends Field {
            constructor(context) {
                super(context);
                this.init();
                this.setStep(0.01);
            }
            init() {
                this.container = document.createElement('div');
                this.container.className = 'field-container';
                let inputContainer = document.createElement('div');
                inputContainer.className = 'input';
                this.container.appendChild(inputContainer);
                this.HTMLElements['input'] = document.createElement('input');
                this.HTMLElements['input'].type = 'number';
                this.HTMLElements['input'].style.textAlign = 'right';
                inputContainer.appendChild(this.HTMLElements['input']);
                this.currency = document.createElement('select');
                inputContainer.appendChild(this.currency);
                this.HTMLElements['label'] = document.createElement('div');
                this.HTMLElements['label'].className = 'field-label';
                this.container.appendChild(this.HTMLElements['label']);
                this.HTMLElements['supportText'] = document.createElement('div');
                this.HTMLElements['supportText'].className = 'field-supportText';
                this.container.appendChild(this.HTMLElements['supportText']);
                this.container.addEventListener('click', function (event) {
                    if (event.target !== this.HTMLElements['input'] && event.target !== this.currency) {
                        this.HTMLElements['input'].focus();
                    }
                }.bind(this), false);
                this.HTMLElements['input'].addEventListener('focus', function () {
                    this.container.className = 'field-container focus';
                }.bind(this), false);
                this.HTMLElements['input'].addEventListener('focusout', this.inputCheck.bind(this), false);
                this.HTMLElements['input'].addEventListener('change', this.onChange.bind(this));
                this.currency.addEventListener('change', this.onChange.bind(this));
            }
            inputCheck() {
                let classes = ['field-container'];
                if (this.HTMLElements['input'].value !== "") {
                    classes.push('filled');
                }
                let check = this.check();
                if (typeof check !== "undefined") {
                    classes.push('error');
                    this.HTMLElements['supportText'].innerText = check;
                }
                else if (typeof this.Hint === "string") {
                    this.HTMLElements['supportText'].innerText = this.Hint;
                }
                else {
                    this.HTMLElements['supportText'].innerText = '';
                }
                this.container.className = classes.join(' ');
            }
            get() {
                return this.container;
            }
            check() {
                if (this.required && !this.HTMLElements['input'].checked) {
                    return ERROR_MSG_REQUIRED;
                }
                if (typeof this.Min === "number" && this.Min > Number(this.HTMLElements['input'].value)) {
                    return "Value must be greater or equal than " + this.Min;
                }
                if (typeof this.Max === "number" && this.Max < Number(this.HTMLElements['input'].value)) {
                    return "Value must be lower or equal than " + this.Max;
                }
                if (typeof this.Step === "number" && parseFloat((Number(this.HTMLElements['input'].value) / this.Step).toPrecision(12)) % 1 !== 0) {
                    return "Value must be in step of " + this.Step;
                }
                return undefined;
            }
            value() {
                return {
                    Value: Number(this.HTMLElements['input'].value),
                    Currency: this.currency.value
                };
            }
            setValue(value) {
                this.HTMLElements['input'].value = value.Value;
                this.currency.value = value.Currency;
                this.inputCheck();
            }
            setPlaceholder(placeholder) {
                this.HTMLElements['input'].placeholder = placeholder;
            }
            setMin(min) {
                this.Min = min;
                this.HTMLElements['input'].min = String(min);
            }
            setMax(max) {
                this.Max = max;
                this.HTMLElements['input'].max = String(max);
            }
            setStep(step) {
                this.Step = step;
                this.HTMLElements['input'].step = String(step);
            }
            setCurrencies(currencies) {
                this.Currencies = currencies;
                while (this.currency.firstChild)
                    this.currency.removeChild(this.currency.firstChild);
                for (let cur of this.Currencies) {
                    let option = document.createElement('option');
                    option.innerText = cur;
                    this.currency.appendChild(option);
                    if (cur === this.InitCurrency) {
                        option.selected = true;
                    }
                }
            }
        }
        Dialog_1.FieldCurrency = FieldCurrency;
        class FieldLocation extends Field {
            constructor(context) {
                super(context);
                this.optionMap = false;
                this.init();
            }
            init() {
                this.container = document.createElement('div');
                this.container.className = 'field-container location';
                let inputContainer = document.createElement('div');
                inputContainer.className = 'input';
                this.container.appendChild(inputContainer);
                this.HTMLElements['containerOptions'] = document.createElement('div');
                this.HTMLElements['containerOptions'].className = 'item';
                this.HTMLElements['containerOptions'].style.display = 'none';
                inputContainer.appendChild(this.HTMLElements['containerOptions']);
                this.HTMLElements['toggle-map'] = document.createElement('button');
                this.HTMLElements['toggle-map'].innerText = 'show map';
                this.HTMLElements['containerOptions'].appendChild(this.HTMLElements['toggle-map']);
                let containerLatitude = document.createElement('div');
                containerLatitude.className = 'item';
                inputContainer.appendChild(containerLatitude);
                this.HTMLElements['latitude-dd'] = document.createElement('input');
                this.HTMLElements['latitude-dd'].type = "number";
                this.HTMLElements['latitude-dd'].placeholder = 'Latitude';
                this.HTMLElements['latitude-dd'].min = '0';
                this.HTMLElements['latitude-dd'].max = '90';
                containerLatitude.appendChild(this.HTMLElements['latitude-dd']);
                this.HTMLElements['latitude-select'] = document.createElement('select');
                this.HTMLElements['latitude-select'].style.width = '45px';
                containerLatitude.appendChild(this.HTMLElements['latitude-select']);
                for (let opt of ['N', 'S']) {
                    let option = document.createElement('option');
                    option.innerText = opt;
                    this.HTMLElements['latitude-select'].appendChild(option);
                }
                let containerLongitude = document.createElement('div');
                containerLongitude.className = 'item';
                inputContainer.appendChild(containerLongitude);
                this.HTMLElements['longitude-dd'] = document.createElement('input');
                this.HTMLElements['longitude-dd'].type = "number";
                this.HTMLElements['longitude-dd'].placeholder = 'Longitude';
                this.HTMLElements['longitude-dd'].min = '0';
                this.HTMLElements['longitude-dd'].max = '180';
                containerLongitude.appendChild(this.HTMLElements['longitude-dd']);
                this.HTMLElements['longitude-select'] = document.createElement('select');
                this.HTMLElements['longitude-select'].style.width = '45px';
                containerLongitude.appendChild(this.HTMLElements['longitude-select']);
                for (let opt of ['E', 'W']) {
                    let option = document.createElement('option');
                    option.innerText = opt;
                    this.HTMLElements['longitude-select'].appendChild(option);
                }
                this.HTMLElements['label'] = document.createElement('div');
                this.HTMLElements['label'].className = 'field-label';
                this.container.appendChild(this.HTMLElements['label']);
                this.HTMLElements['supportText'] = document.createElement('div');
                this.HTMLElements['supportText'].className = 'field-supportText';
                this.container.appendChild(this.HTMLElements['supportText']);
                this.HTMLElements['mapContainer'] = document.createElement('div');
                this.HTMLElements['mapContainer'].style.display = 'none';
                this.container.appendChild(this.HTMLElements['mapContainer']);
                let mapOptions = {
                    center: [53.52, 10],
                    scrollWheelZoom: false,
                    zoom: 10,
                };
                this.map = new L.map(this.HTMLElements['mapContainer'], mapOptions);
                this.layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
                this.map.addLayer(this.layer);
                this.map.on('click', function (event) {
                    this.setValue({
                        Latitude: event.latlng.lat,
                        Longitude: event.latlng.lng
                    });
                    this.inputCheck();
                    if (this.map)
                        this.updateMarkerPosition();
                }.bind(this));
                this.container.addEventListener('click', function (event) {
                    if (event.target === this.container) {
                        this.HTMLElements['latitude-dd'].focus();
                    }
                    else if (event.target === this.HTMLElements['latitude-select']
                        || event.target == this.HTMLElements['longitude-select']) {
                        this.inputCheck();
                    }
                }.bind(this), false);
                this.HTMLElements['latitude-dd'].addEventListener('focus', function () {
                    this.container.className = 'field-container location focus';
                }.bind(this), false);
                this.HTMLElements['longitude-dd'].addEventListener('focus', function () {
                    this.container.className = 'field-container location focus';
                }.bind(this), false);
                this.HTMLElements['latitude-dd'].addEventListener('focusout', this.inputCheck.bind(this), false);
                this.HTMLElements['longitude-dd'].addEventListener('focusout', this.inputCheck.bind(this), false);
                this.HTMLElements['latitude-select'].addEventListener('focusout', this.inputCheck.bind(this), false);
                this.HTMLElements['longitude-select'].addEventListener('focusout', this.inputCheck.bind(this), false);
                this.HTMLElements['latitude-dd'].addEventListener('change', this.onChange.bind(this));
                this.HTMLElements['longitude-dd'].addEventListener('change', this.onChange.bind(this));
                this.HTMLElements['latitude-select'].addEventListener('change', this.onChange.bind(this));
                this.HTMLElements['longitude-select'].addEventListener('change', this.onChange.bind(this));
                this.HTMLElements['toggle-map'].addEventListener('click', function () {
                    if (this.HTMLElements['toggle-map'].innerText === 'show map') {
                        this.HTMLElements['toggle-map'].innerText = 'hide map';
                        this.HTMLElements['mapContainer'].style.display = '';
                        this.onShow();
                        this.inputCheck();
                    }
                    else {
                        this.HTMLElements['toggle-map'].innerText = 'show map';
                        this.HTMLElements['mapContainer'].style.display = 'none';
                    }
                }.bind(this), false);
            }
            onChange() {
                super.onChange();
                if (this.map)
                    this.updateMarkerPosition();
            }
            onShow() {
                this.map.invalidateSize();
            }
            inputCheck() {
                let classes = ['field-container', 'location'];
                if (this.HTMLElements['latitude-dd'].value !== ''
                    || this.HTMLElements['longitude-dd'].value !== ''
                    || this.HTMLElements['latitude-select'] === document.activeElement
                    || this.HTMLElements['longitude-select'] === document.activeElement
                    || this.HTMLElements['toggle-map'].innerText === 'hide map') {
                    classes.push('filled');
                }
                let check = this.check();
                if (typeof check !== "undefined") {
                    classes.push('error');
                    this.HTMLElements['supportText'].innerText = check;
                }
                else if (typeof this.Hint === "string") {
                    this.HTMLElements['supportText'].innerText = this.Hint;
                }
                else {
                    this.HTMLElements['supportText'].innerText = '';
                }
                this.container.className = classes.join(' ');
            }
            get() {
                setTimeout(function () {
                    this.map.invalidateSize();
                }.bind(this), 200);
                return this.container;
            }
            check() {
                if (this.required && (this.HTMLElements['latitude-dd'].value === '' || this.HTMLElements['longitude-dd'].value === '')) {
                    return ERROR_MSG_REQUIRED;
                }
                if (Number(this.HTMLElements['latitude-dd'].value) < 0 || Number(this.HTMLElements['latitude-dd'].value) > 90) {
                    return "The Latitude must be between 0° & 90°";
                }
                if (this.HTMLElements['longitude-dd'].value < 0 || this.HTMLElements['longitude-dd'].value > 180) {
                    return "The Longitude must be between 0° & 180°";
                }
                return undefined;
            }
            value() {
                if (typeof this.check() === 'undefined'
                    && this.HTMLElements['latitude-dd'].value !== ''
                    && this.HTMLElements['longitude-dd'].value !== '') {
                    return {
                        Latitude: Number(this.HTMLElements['latitude-dd'].value) * (Number(this.HTMLElements['latitude-select'].value === 'N') * 2 - 1),
                        Longitude: Number(this.HTMLElements['longitude-dd'].value) * (Number(this.HTMLElements['longitude-select'].value === 'E') * 2 - 1),
                    };
                }
                return undefined;
            }
            setValue(value) {
                this.HTMLElements['latitude-dd'].value = Math.abs(value.Latitude);
                this.HTMLElements['longitude-dd'].value = Math.abs(value.Longitude);
                this.HTMLElements['latitude-select'].value = (value.Latitude >= 0) ? 'N' : 'S';
                this.HTMLElements['longitude-select'].value = (value.Longitude >= 0) ? 'E' : 'W';
                this.inputCheck();
                this.updateMarkerPosition();
            }
            setMap(map) {
                this.optionMap = map;
                if (this.optionMap) {
                    this.HTMLElements['containerOptions'].style.display = '';
                }
                else {
                    this.HTMLElements['containerOptions'].style.display = 'none';
                }
            }
            updateMarkerPosition() {
                let position = [
                    Number(this.HTMLElements['latitude-dd'].value) * (Number(this.HTMLElements['latitude-select'].value === 'N') * 2 - 1),
                    Number(this.HTMLElements['longitude-dd'].value) * (Number(this.HTMLElements['longitude-select'].value === 'E') * 2 - 1)
                ];
                if (typeof this.marker === 'undefined') {
                    this.marker = L.marker([0, 0]).addTo(this.map);
                }
                this.marker.setLatLng(position);
                this.map.panTo(position);
            }
        }
        Dialog_1.FieldLocation = FieldLocation;
        const ADDRESS_DEFINITION = {
            "Austria": {
                "default": {
                    streetNumber: { label: "Number" },
                    streetNumberSuffix: { label: "Suffix" },
                    streetName: { label: "Street" },
                    city: { label: "City" },
                    postalArea: { label: "Postal Area", check: "^\\d{4}$", checkHint: "4 digits" },
                },
            },
            "Belgium": {
                "default": {
                    streetNumber: { label: "Number" },
                    streetNumberSuffix: { label: "Suffix" },
                    streetName: { label: "Street" },
                    city: { label: "City" },
                    postalArea: { label: "Postal Area", check: "^\\d{4}$", checkHint: "4 digits" },
                },
            },
            "Czech Republic": {
                "default": {
                    streetNumber: { label: "Number" },
                    streetNumberSuffix: { label: "Suffix" },
                    streetName: { label: "Street" },
                    city: { label: "City" },
                    postalArea: { label: "Postal Area", check: "^\\d{3}\\s\\d{2}$", checkHint: "e.g 115 03" },
                },
            },
            "Denmark": {
                "default": {
                    streetNumber: { label: "Number" },
                    streetNumberSuffix: { label: "Additional" },
                    streetName: { label: "Street" },
                    city: { label: "City" },
                    postalArea: { label: "Postal Code", check: "^\\d{4}$", checkHint: "4 digits" },
                },
            },
            "Germany": {
                "default": {
                    streetNumber: { label: "Number" },
                    streetNumberSuffix: { label: "Suffix" },
                    streetName: { label: "Street" },
                    city: { label: "City" },
                    postalArea: { label: "Postal Code", check: "^\\d{5}$", checkHint: "5 digits" },
                },
                "PO-BOX": {
                    addressTypeIdentifier: { label: "Post box" },
                    city: { label: "City" },
                    postalArea: { label: "Postal Code", check: "^\\d{5}$", checkHint: "5 digits" },
                },
            },
            "Estonia": {
                "default": {
                    streetNumber: { label: "Number" },
                    streetNumberSuffix: { label: "Additional" },
                    streetName: { label: "Street" },
                    city: { label: "City" },
                    postalArea: { label: "Postal Code", check: "^\\d{5}$", checkHint: "5 digits" },
                },
            },
            "Finland": {
                "default": {
                    streetNumber: { label: "Number" },
                    streetNumberSuffix: { label: "Additional" },
                    streetName: { label: "Street" },
                    city: { label: "City" },
                    postalArea: { label: "Postal Code", check: "^\\d{5}$", checkHint: "5 digits" },
                },
            },
            "France": {
                "default": {
                    streetNumber: { label: "Number" },
                    streetNumberSuffix: { label: "Additional" },
                    streetName: { label: "Street" },
                    city: { label: "City" },
                    postalArea: { label: "Postal Code", check: "^\\d{5}$", checkHint: "5 digits" },
                },
            },
            "Iceland": {
                "default": {
                    streetNumber: { label: "Number" },
                    streetNumberSuffix: { label: "Suffix" },
                    streetName: { label: "Street" },
                    city: { label: "City" },
                    postalArea: { label: "Postal Area", check: "^\\d{3}$", checkHint: "3 digits" },
                },
                "PO-BOX": {
                    addressTypeIdentifier: { label: "Post box" },
                    city: { label: "City" },
                    postalArea: { label: "Postal Area", check: "^\\d{3}$", checkHint: "3 digits" },
                },
            },
            "Ireland": {
                "default": {
                    streetNumber: { label: "Number" },
                    streetNumberSuffix: { label: "Additional" },
                    streetName: { label: "Street" },
                    city: { label: "City" },
                    postalArea: { label: "Postal Code", check: "^[A-Z0-9]{3}\\s[A-Z0-9]{3}$", checkHint: "e. g. A65 F4E2" },
                },
            },
            "Italy": {
                "default": {
                    streetNumber: { label: "Number" },
                    streetNumberSuffix: { label: "Suffix" },
                    streetName: { label: "Street" },
                    city: { label: "City" },
                    governingDistrict: { label: "Provice Code", check: "^[A-Z]{2}$", checkHint: "2 uppercase letters" },
                    postalArea: { label: "Postal Code", check: "^\\d{5}$", checkHint: "5 digits" },
                },
                "PO-BOX": {
                    addressTypeIdentifier: { label: "Post box" },
                    city: { label: "City" },
                    governingDistrict: { label: "Provice Code", check: "^[A-Z]{2}$", checkHint: "2 uppercase letters" },
                    postalArea: { label: "Postal Code", check: "^\\d{5}$", checkHint: "5 digits" },
                },
                "kilometric civic number": {
                    addressTypeIdentifier: { label: "Kilometer", check: "^\\d+$", checkHint: "only digits" },
                    city: { label: "City" },
                    governingDistrict: { label: "Provice Code", check: "^[A-Z]{2}$", checkHint: "2 uppercase letters" },
                    postalArea: { label: "Postal Code", check: "^\\d{5}$", checkHint: "5 digits" },
                },
            },
            "Luxembourg": {
                "default": {
                    streetNumber: { label: "Number" },
                    streetNumberSuffix: { label: "Additional" },
                    streetName: { label: "Street" },
                    city: { label: "City" },
                    postalArea: { label: "Postal Code", check: "^\\d{4}$", checkHint: "4 digits" },
                },
                "PO-BOX": {
                    addressTypeIdentifier: { label: "Post box" },
                    city: { label: "City" },
                    postalArea: { label: "Postal Code", check: "^\\d{4}$", checkHint: "4 digits" },
                },
            },
            "Netherlands": {
                "default": {
                    streetNumber: { label: "Number" },
                    streetNumberSuffix: { label: "Suffix" },
                    streetName: { label: "Street" },
                    city: { label: "City" },
                    postalArea: { label: "Postal Code", check: "^\\d{4}$", checkHint: "4 digits" },
                    governingDistrict: { label: "Region Code", check: "^[A-Z]{2}$", checkHint: "2 uppercase letters" },
                },
                "PO-BOX": {
                    addressTypeIdentifier: { label: "Post box" },
                    city: { label: "City" },
                    postalArea: { label: "Postal Code", check: "^\\d{5}$", checkHint: "5 digits" },
                    governingDistrict: { label: "Region Code", check: "^[A-Z]{2}$", checkHint: "2 uppercase letters" },
                },
                "business-reply": {
                    addressTypeIdentifier: { label: "Post box" },
                    city: { label: "City" },
                    postalArea: { label: "Postal Code", check: "^\\d{5}$", checkHint: "5 digits" },
                    governingDistrict: { label: "Region Code", check: "^[A-Z]{2}$", checkHint: "2 uppercase letters" },
                },
            },
            "Norway": {
                "default": {
                    streetNumber: { label: "Number" },
                    streetNumberSuffix: { label: "Additional" },
                    streetName: { label: "Street" },
                    city: { label: "City" },
                    postalArea: { label: "Postal Code", check: "^\\d{4}$", checkHint: "4 digits" },
                },
                "PO-BOX": {
                    addressTypeIdentifier: { label: "Post box" },
                    city: { label: "City" },
                    postalArea: { label: "Postal Code", check: "^\\d{4}$", checkHint: "4 digits" },
                },
            },
            "Poland": {
                "default": {
                    streetNumber: { label: "Number" },
                    streetNumberSuffix: { label: "Suffix" },
                    streetName: { label: "Street" },
                    city: { label: "City" },
                    postalArea: { label: "Postal Area", check: "^\\d{2}-\\d{3}$", checkHint: "e.g. 00-940" },
                },
            },
            "Portugal": {
                "default": {
                    streetNumber: { label: "Number" },
                    streetNumberSuffix: { label: "Suffix" },
                    streetName: { label: "Street" },
                    city: { label: "City" },
                    postalArea: { label: "Postal Area", check: "^\\d{4}-\\d{3}$", checkHint: "e.g. 1167-921" },
                },
            },
            "Spain": {
                "default": {
                    streetNumber: { label: "Number" },
                    streetNumberSuffix: { label: "Suffix" },
                    streetName: { label: "Street" },
                    city: { label: "City" },
                    postalArea: { label: "Postal Code", check: "^\\d{5}$", checkHint: "5 digits" },
                },
            },
            "Sweden": {
                "default": {
                    streetNumber: { label: "Number" },
                    streetNumberSuffix: { label: "Additional" },
                    streetName: { label: "Street" },
                    city: { label: "City" },
                    postalArea: { label: "Postal Code", check: "^\\d{3}\\s\\d{2}$", checkHint: "e.g. 113 49" },
                },
            },
            "Switzerland": {
                "default": {
                    streetNumber: { label: "Number" },
                    streetNumberSuffix: { label: "Suffix" },
                    streetName: { label: "Street" },
                    city: { label: "City" },
                    postalArea: { label: "Postal Code", check: "^\\d{4}$", checkHint: "4 digits" },
                },
                "PO-BOX": {
                    addressTypeIdentifier: { label: "Post box" },
                    city: { label: "City" },
                    postalArea: { label: "Postal Code", check: "^\\d{4}$", checkHint: "4 digits" },
                },
            },
            "United Kingdom": {
                "default": {
                    streetNumber: { label: "Number" },
                    streetNumberSuffix: { label: "Suffix" },
                    streetName: { label: "Street" },
                    city: { label: "City" },
                    postalArea: { label: "Postal Code", check: "^[A-Z0-9]{4}\\s[A-Z0-9]{3}$", checkHint: "e. g. HP19 3EQ" },
                },
            },
        };
        class FieldAddress extends Field {
            constructor(context) {
                super(context);
                this.subFields = [];
                this.init();
            }
            init() {
                this.container = document.createElement('div');
                this.container.className = 'field-address';
                this.HTMLElements['label'] = document.createElement('h4');
                this.HTMLElements['label'].className = 'label';
                this.HTMLElements['label'].innerText = 'Address';
                this.container.appendChild(this.HTMLElements['label']);
                this.subFields['country'] = new FieldSelect(this.context);
                this.subFields['country'].setLabel('Country');
                this.container.appendChild(this.subFields['country'].get());
                for (let country in ADDRESS_DEFINITION) {
                    this.subFields['country'].appendItem({
                        headline: country,
                        value: country
                    });
                }
                this.subFields['type'] = new FieldSelect(this.context);
                this.subFields['type'].setLabel('Address Type');
                this.container.appendChild(this.subFields['type'].get());
                this.subFields['streetName'] = new FieldText(this.context);
                this.container.appendChild(this.subFields['streetName'].get());
                this.subFields['streetName'].setMaxLength(100);
                this.subFields['streetName'].setLabel('Street');
                this.subFields['houseName'] = new FieldText(this.context);
                this.container.appendChild(this.subFields['houseName'].get());
                this.subFields['houseName'].setMaxLength(100);
                this.subFields['houseName'].setLabel('House Name');
                this.subFields['streetNumber'] = new FieldNumber(this.context);
                this.container.appendChild(this.subFields['streetNumber'].get());
                this.subFields['streetNumber'].setLabel('Street Number');
                this.subFields['streetNumber'].get().style.gridColumn = 'span 2';
                this.subFields['streetNumberSuffix'] = new FieldText(this.context);
                this.container.appendChild(this.subFields['streetNumberSuffix'].get());
                this.subFields['streetNumberSuffix'].setMaxLength(16);
                this.subFields['streetNumberSuffix'].setLabel('Number Suffix');
                this.subFields['streetNumberSuffix'].get().style.gridColumn = 'span 2';
                this.subFields['streetType'] = new FieldText(this.context);
                this.container.appendChild(this.subFields['streetType'].get());
                this.subFields['streetType'].setMaxLength(64);
                this.subFields['streetType'].setLabel('Street Type');
                this.subFields['addressTypeIdentifier'] = new FieldText(this.context);
                this.container.appendChild(this.subFields['addressTypeIdentifier'].get());
                this.subFields['addressTypeIdentifier'].setMaxLength(100);
                this.subFields['addressTypeIdentifier'].setLabel('Address Type Identifier');
                this.subFields['localMuniciplity'] = new FieldText(this.context);
                this.container.appendChild(this.subFields['localMuniciplity'].get());
                this.subFields['localMuniciplity'].setMaxLength(100);
                this.subFields['localMuniciplity'].setLabel('Local Municiplity');
                this.subFields['city'] = new FieldText(this.context);
                this.container.appendChild(this.subFields['city'].get());
                this.subFields['city'].setMaxLength(100);
                this.subFields['city'].setLabel('City');
                this.subFields['governingDistrict'] = new FieldText(this.context);
                this.container.appendChild(this.subFields['governingDistrict'].get());
                this.subFields['governingDistrict'].setMaxLength(100);
                this.subFields['governingDistrict'].setLabel('Governing District');
                this.subFields['postalArea'] = new FieldText(this.context);
                this.container.appendChild(this.subFields['postalArea'].get());
                this.subFields['postalArea'].setMaxLength(100);
                this.subFields['postalArea'].setLabel('Postal Area');
                this.subFields['country'].onChange = function () {
                    this.onDefinitionChange(true);
                }.bind(this);
                this.subFields['type'].onChange = function () {
                    this.onDefinitionChange(false);
                }.bind(this);
                this.subFields['country'].setValue('Germany');
                this.onDefinitionChange();
            }
            onDefinitionChange(addressChange = true) {
                if (addressChange) {
                    this.subFields['type'].clearItems();
                    for (let type in ADDRESS_DEFINITION[this.subFields['country'].value()]) {
                        this.subFields['type'].appendItem({
                            headline: type,
                            value: type
                        });
                    }
                    this.subFields['type'].setValue('default');
                }
                let address = ADDRESS_DEFINITION[this.subFields['country'].value()][this.subFields['type'].value()];
                for (let key of ['streetName', 'houseName', 'streetNumber', 'streetType', 'streetNumberSuffix', 'addressTypeIdentifier', 'localMuniciplity', 'city', 'governingDistrict', 'postalArea']) {
                    if (this.subFields[key] instanceof Field) {
                        if (typeof address[key] === 'object') {
                            this.subFields[key].get().style.display = '';
                            if (typeof address[key].label === "string") {
                                this.subFields[key].setLabel(address[key].label);
                            }
                            else {
                                this.subFields[key].setLabel(key);
                            }
                            if (typeof address[key].check === "string") {
                                if (this.subFields[key] instanceof FieldText) {
                                    this.subFields[key].setCheck(address[key].check, address[key].checkHint);
                                }
                                else {
                                    this.subFields[key].setCheck(undefined, undefined);
                                }
                            }
                        }
                        else {
                            this.subFields[key].get().style.display = 'none';
                        }
                    }
                }
            }
            get() {
                return this.container;
            }
            value() {
                let address = {};
                for (let name in this.subFields) {
                    let value = this.subFields[name].value();
                    if (typeof value !== 'undefined') {
                        address[name] = value;
                    }
                }
                return address;
            }
            setStreetNumber(streetNumber) {
                this.subFields['streetNumber'].setValue(streetNumber);
            }
            setHouseName(houseName) {
                this.subFields['houseName'].setValue(houseName);
            }
            setStreetNumberSuffix(streetNumberSuffix) {
                this.subFields['streetNumberSuffix'].setValue(streetNumberSuffix);
            }
            setStreetName(streetName) {
                this.subFields['streetName'].setValue(streetName);
            }
            setStreetType(streetType) {
                this.subFields['streetType'].setValue(streetType);
            }
            setStreetDirection(streetDirection) {
                this.subFields['streetDirection'].setValue(streetDirection);
            }
            setAddressType(addressType) {
                this.subFields['type'].setValue(addressType);
            }
            setAddressTypeIdentifier(addressTypeIdentifier) {
                this.subFields['addressTypeIdentifier'].setValue(addressTypeIdentifier);
            }
            setLocalMuniciplity(localMuniciplity) {
                this.subFields['localMuniciplity'].setValue(localMuniciplity);
            }
            setCity(city) {
                this.subFields['city'].setValue(city);
            }
            setGoverningDistrict(governingDistrict) {
                this.subFields['governingDistrict'].setValue(governingDistrict);
            }
            setPostalArea(postalArea) {
                this.subFields['postalArea'].setValue(postalArea);
            }
            setCountry(country) {
                this.subFields['country'].setValue(country);
            }
        }
        Dialog_1.FieldAddress = FieldAddress;
        class FieldFileSelector extends Field {
            constructor(context) {
                super(context);
                this.init();
            }
            init() {
                this.container = document.createElement('div');
                this.container.className = 'field-container fileSelect filled';
                this.HTMLElements['label'] = document.createElement('div');
                this.HTMLElements['label'].className = 'field-label';
                this.container.appendChild(this.HTMLElements['label']);
                this.HTMLElements['file-container'] = document.createElement('div');
                this.HTMLElements['file-container'].className = 'file-container';
                this.container.appendChild(this.HTMLElements['file-container']);
            }
            get() {
                return this.container;
            }
            value() {
                return this.selected;
            }
            setSource(source) {
                this.source = source;
                this.getFiles();
            }
            setValue(value) {
                this.selected = value;
            }
            getFiles(callback = undefined) {
                fetch(this.source)
                    .then((response) => response.json())
                    .then((data) => {
                    if (typeof data === 'object' && data !== null) {
                        for (let item of data) {
                            let file = document.createElement('div');
                            file.className = 'file';
                            file.innerText = item.title;
                            this.HTMLElements['file-container'].appendChild(file);
                            if (this.selected === item.value) {
                                file.className = 'file selected';
                                this.selectedItem = file;
                            }
                            file.addEventListener('click', function () {
                                this.selected = item.value;
                                if (this.selectedItem instanceof HTMLDivElement)
                                    this.selectedItem.className = 'file';
                                file.className = 'file selected';
                                this.selectedItem = file;
                            }.bind(this));
                        }
                    }
                    if (typeof callback === 'function')
                        callback();
                });
            }
        }
        Dialog_1.FieldFileSelector = FieldFileSelector;
        class Button {
            constructor(context) {
                this.context = context;
                this.element = document.createElement('button');
                this.element.addEventListener('click', this.onClick.bind(this));
            }
            get() {
                return this.element;
            }
            onClick() {
                switch (this.action.context) {
                    case 'dialog':
                        if (this.action.value.substring(0, 4) === 'view') {
                            this.context.setView(this.action.value.substring(5));
                        }
                        else if (this.action.value === 'back') {
                            this.context.setView(this.context.viewHistory.pop(), false);
                        }
                        break;
                    case 'submit':
                        fetch(this.action.value, {
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/json'
                            },
                            body: JSON.stringify(this.context.value())
                        }).then((response) => response.json()).then((response) => {
                            switch (response.status) {
                                case 200:
                                    this.context.close(false);
                                    break;
                                case 302:
                                    if (typeof response.location === 'string') {
                                        window.location.href = response.location;
                                    }
                                    else {
                                        console.error('status 300 need location for redirect');
                                    }
                                    break;
                                default:
                                    let msg = `Error ${response.status}`;
                                    if (typeof response.errorMsg === 'string') {
                                        msg += ': ' + response.errorMsg;
                                    }
                                    this.context.showError(msg);
                                    console.error(response);
                            }
                        }).catch(error => {
                            this.context.showError(`Error: ${error}`);
                        });
                        break;
                }
            }
            setLabel(label) {
                this.element.innerText = label;
            }
            setAction(action) {
                let parts = action.split(':');
                this.action = {
                    context: parts[0],
                    value: parts[1],
                };
            }
            setVisibility(visibility) {
                this.visibility = visibility;
                if (visibility) {
                    this.element.style.display = '';
                }
                else {
                    this.element.style.display = 'none';
                }
            }
            setRequirements(requirements) {
                this.requirements = requirements;
            }
            checkRequirements() {
                if (typeof this.requirements !== "undefined") {
                    if (this.requirements.check()) {
                        this.setVisibility(true);
                    }
                    else {
                        this.setVisibility(false);
                    }
                }
            }
        }
        Dialog_1.Button = Button;
    })(Dialog = Maknapp.Dialog || (Maknapp.Dialog = {}));
})(Maknapp || (Maknapp = {}));
//# sourceMappingURL=Maknapp_Dialog.js.map